"""generic utils"""
from __future__ import annotations
import ast
import torch as tr
from argparse import Namespace
from typing import Any, T, Iterable
from itertools import chain, combinations
from omegaconf import DictConfig, ListConfig
from loggez import loggez_logger as logger

def oc_argparse_overwrite(cfg: DictConfig, args: Namespace) -> DictConfig:
    """manually replace an OmegaConf node from the CLI via argparse"""
    for item in args.replace_cfg:
        assert len(item) == 2, f"Expected [cfg.]x.y.z [=] value, got {item}"
        left, right = item[0].split("."), ast.literal_eval(item[1])
        _cfg = cfg
        for k in left[0:-1]:
            assert k in _cfg, f"Key {k} of {item} not in {cfg}"
            _cfg = _cfg[k]
        assert left[-1] in _cfg, f"Key {left[-1]} of {item} not in {cfg}"
        logger.info(f"cfg.{item[0]} ({_cfg[left[-1]]}) replaced by CLI: {right}")
        _cfg[left[-1]] = right
    return cfg

def powerset(s: list[T]) -> list[list[T]]:
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    return list(chain.from_iterable(combinations(s, r) for r in range(len(s) + 1)))[1:]

def mean(x: Iterable[T]) -> T:
    """mean of any iterable"""
    return sum(x) / len(x)

def is_sorted(lst: list | ListConfig) -> bool:
    """checks that a list is sorted"""
    return sorted(lst) == [*lst] # works for omegaconf too

def flatten_list(x: list[list[str]]) -> list[str]:
    """Flattens a list of lists of strings."""
    if len(x) == 0:
        return []
    assert not isinstance(x, (tuple, set)), x
    res = []
    for item in x:
        if isinstance(item, (list, ListConfig)):
            res.extend(flatten_list(item))
        else:
            assert isinstance(item, str), (item, type(item))
            res.append(item)
    return res

def split_pred_tensor(pred: tr.Tensor, task_ch: dict[str, int]) -> dict[str, tr.Tensor]:
    """Given a prediction tensor and a dict: task->n_channels, split the predictions. Returns a dict of tensors."""
    n_so_far, res = 0, {}
    assert (A := pred.shape[1]) == (B := sum(task_ch.values())), (A, B) # (B, sum(ch), H, W)
    for task, ch in task_ch.items():
        res[task] = pred[:, n_so_far: n_so_far + ch]
        n_so_far += ch
    return res

class MovingAverage:
    """Moving average class. Based on: https://claude.ai/chat/bcb05cd9-5eb8-4a90-8cfe-8c44ba0707ba."""
    def __init__(self, beta: float = 0.9):
        self.beta = beta
        self.avg: Any = None

    def update(self, value: Any) -> Any:
        """update this moving average and return the result"""
        if self.avg is None:
            self.avg = value
        else:
            self.avg = self.beta * self.avg + (1 - self.beta) * value
        return self.avg

    def __repr__(self):
        return f"MovingAverage: {self.avg}"
