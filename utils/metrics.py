"""metrics and loss computation algorithms"""
from __future__ import annotations
from typing import Callable
from omegaconf import DictConfig
from functools import partial
import torch as tr
from torch.nn import functional as F
from lightning_module_enhanced.metrics import CoreMetric, CallableCoreMetric, MeanIoU
from loggez import loggez_logger as logger

from .patch_tensor import patch_tensor

MetricFn = Callable[[tr.Tensor, tr.Tensor, tr.Tensor], tr.Tensor]
MetricsOutput = tuple[dict[str, tr.Tensor], dict[str, int]] # tensor & valid counts

# metrics basic functions (y, gt, mask) -> FloatTensor
def l2(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    """y :: (B, C, -1), gt :: (B, C, -1) -> float. No NaNs allowed"""
    assert not y.isnan().any() and not gt.isnan().any(), (y, gt)
    return (y - gt).pow(2).mean()

def ce(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    """y :: (B, C, -1), gt :: (B, C, -1) -> float. No NaNs allowed"""
    assert not y.isnan().any() and not gt.isnan().any(), (y, gt)
    gt_argmax = gt.argmax(dim=1) # cross entropy expects class indexes. Expected channels on dim 1! (see mae_algorithms)
    return F.cross_entropy(y, gt_argmax)

def bce(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    """y :: (B, 2, -1), gt :: (B, 2, -1) -> float. No NaNs allowed"""
    assert not y.isnan().any() and not gt.isnan().any(), (y, gt)
    return F.binary_cross_entropy(y.sigmoid(), gt)

def kl(y: tr.Tensor, gt: tr.Tensor, T: float) -> tr.Tensor:
    """
    y :: (B, C, -1), gt :: (B, C, -1) -> float. No NaNs allowed. The data must be in un-normalized logits.
    Apparently, we need to apply log_softmax ourselves to y (student) and softmax to gt (teacher).
    More notes: https://chat.deepseek.com/a/chat/s/4b8eaa19-1dc8-4c99-9e53-98c86749a511.
    """
    assert not y.isnan().any() and not gt.isnan().any(), (y, gt)
    teacher_probs = F.softmax(gt / T, dim=1)
    student_probs = F.log_softmax(y / T, dim=1)
    res = F.kl_div(student_probs, teacher_probs, reduction="none") * (T ** 2)
    return res.sum(1).mean()

def compute_metrics(y: dict[str, tr.Tensor], gt: dict[str, tr.Tensor], masks: dict[str, tr.Tensor] | None,
                    metrics: dict[str, list[CoreMetric]]) -> dict[str, dict[str, tr.Tensor]]:
    y_pt = {k: patch_tensor(v, gt[k].shape[-3:-1]) for k, v in y.items()}
    y_metrics, gt_metrics, = y_pt, gt
    if masks is not None:
        y_metrics = {k: v[..., masks[k]] for k, v in y_pt.items()}
        gt_metrics = {k: v[..., masks[k]] for k, v in gt.items()}
    tasks, done_metrics = sorted(y.keys(), reverse=True), set() # sort to avoid clashes like LSTD_AN_l2 and LSTD_l2

    res = {}
    for task in tasks:
        B, C = gt[task].shape[0:2]
        task_metrics = [metric for metric in metrics if metric.startswith(f"{task}_") and metric not in done_metrics]
        y_task, gt_task = y_metrics[task], gt_metrics[task]
        gt_nan = gt_task.isnan()
        if (~gt_nan).sum() == 0 or y_metrics[task].shape[-1] == 0:
            res = {**res, **{m: None for m in task_metrics}}
            continue
        for task_metric in task_metrics:
            try:
                y_M, gt_M = y_task[~gt_nan].reshape(B, C, -1), gt_task[~gt_nan].reshape(B, C, -1)
                res[task_metric] = metrics[task_metric](y_M, gt_M)
            except:
                res[task_metric] = None

        done_metrics.update(task_metrics)
    return res

def _build_metric_from_cfg(metric_cfg: DictConfig) -> Callable:
    supported_metrics = {"l2": l2, "ce": ce, "mean_iou": MeanIoU, "bce": bce, "kl": kl}
    metric, params = supported_metrics[metric_cfg["name"]], metric_cfg["parameters"]
    return metric(**params) if isinstance(metric, type) else partial(metric, **params) # MeanIoU(params) vs partial(...)

def build_metrics(metrics_cfg: DictConfig, name_to_task: dict[str, "Task"]) -> dict[str, CoreMetric]:
    """function that builds the metrics computer for training based on the yaml ml config file"""
    assert metrics_cfg.type in ("l2_regression_ce_classification", ), metrics_cfg.type # TOOD: per_task?
    if metrics_cfg.type == "l2_regression_ce_classification":
        metric_fns = {}
        for k, v in name_to_task.items():
            if v.is_classification:
                metric_fns[k] = {"ce": ce} if v.is_classification and len(v.classes) > 2 else {"bce": bce}
            else:
                metric_fns[k] = {"l2": l2}
        if (ow := metrics_cfg.metrics_overwrites) is not None:
            for k, v in ow.items():
                assert k in name_to_task.keys(), (k, name_to_task)
                assert len(v) > 0 and all(["name" in _v and "parameters" in _v for _v in v]), (k, v)
                logger.info(f"Overwriting metrics for task '{k}': {[_v.name for _v in v]}")
                metric_fns[k] = {_v["name"]: _build_metric_from_cfg(_v) for _v in v}
    res = {}
    for task in metric_fns.keys():
        for metric_name, metric_fn in metric_fns[task].items():
            if not isinstance(metric_fn, CoreMetric):
                logger.debug(f"'{task}_{metric_name}' is not CoreMetric. Converting")
                metric_fn = CallableCoreMetric(metric_fn, higher_is_better=False)
            # our naming convention is ambigous. This checks for [LSTD_AN]_[l2] vs [LSTD]_[AN_l2]. sorted() above too.
            assert f"{task}_{metric_name}" not in res, f"'{task}_{metric_name}' already exists in {res}"
            res[f"{task}_{metric_name}"] = metric_fn
            if isinstance(metric_fn, MeanIoU) and metric_fn.class_axis != 1: # sanity check so we don't ge bugs later on
                raise ValueError(f"Metric {task}_{metric_name} is MeanIoU with {metric_fn.class_axis=} that is not 1!")
    return res
