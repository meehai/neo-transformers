"""masking class that should be used for all the masking algorithms"""
from __future__ import annotations
from random import random, choice
import sys
from pathlib import Path
from itertools import product
from omegaconf import DictConfig, ListConfig
from typing import Callable, Any
import pytest
from loggez import loggez_logger as logger
sys.path.insert(0, Path(__file__).parents[1].__str__())
from utils import powerset

class Combo(list):
    """Helper class that extends a regular list, but provides semantics for a set of input-output tasks masking list"""
    def __init__(self, input_tasks: list[str], output_tasks: list[str]):
        self.input_tasks = input_tasks
        self.output_tasks = output_tasks
        assert set(self.input_tasks).intersection(self.output_tasks) == set(), (input_tasks, output_tasks)
        assert len(input_tasks) >= 1 and len(output_tasks) >= 1, (input_tasks, output_tasks)

    def to_dict(self) -> dict[str, float]:
        """returns a dict with {task: drop_percent} for all tasks in this combo"""
        res = {**{k: 0 for k in self.input_tasks}, **{k: 100 for k in self.output_tasks}}
        return res

    def __repr__(self):
        return f"[{','.join(self.input_tasks)} -> {','.join(self.output_tasks)}]"

    def __str__(self):
        return repr(self)

    def __hash__(self):
        return hash(tuple(sorted(set(self.input_tasks)))) + hash(tuple(sorted(set(self.output_tasks))))

def build_masking_algorithm(masking_type: str, masking_params: DictConfig[str, Any]) -> MaskingAlgorithm:
    """build the masking algorithm. For now, it can be 'random_masking' or 'fixed_masking' (provided list)"""
    res = None
    if masking_type == "random_masking":
        res = RandomMasking(**masking_params)
    if masking_type in ("fixed_masking", "fixed_ordered_masking"):
        assert isinstance(fml := masking_params.fixed_masking_list, (list, ListConfig)), f"{fml} not list {type(fml)}"
        assert isinstance(et := masking_params.excluded_tasks, (list, ListConfig)), f"{et} is not a list: {type(et)}"
        assert (len(fml) == 0) ^ (len(et) == 0), f"Only one must be set at a time:\n-{fml}\n-{et}"
        tasks = masking_params.tasks
        class_type = {"fixed_masking": FixedMasking, "fixed_ordered_masking": FixedOrderedMasking}[masking_type]
        if len(fml) == 0:
            ot = masking_params.pop("excluded_tasks")
            it = [task for task in tasks if task not in ot]
            assert len(ot) > 0, "For fixed masking, we need at least one output tasks (via excluded_tasks entry)"
            assert all(ot in tasks for ot in ot), (ot, tasks)
            assert len(it) > 0, f"Expected at least one input task. All are output: {tasks} -- {ot}"
            res = class_type.build_from_tasks(it, ot)
        else:
            res = class_type(combos=[Combo(*combo) for combo in fml])
            logger.info(f"Provided combos: {res.combos}")
        for combo in res.combos:
            assert set(combo.input_tasks + combo.output_tasks).issubset(tasks), \
                f"some tasks of {combo} not in {tasks}. Check the dependencies in MultiTaskReader.task_types."
    assert res is not None, f"Unknown masking_type provided: '{masking_type}'"
    logger.info(f"Built masking algorithm: {res}")
    return res

class MaskingAlgorithm(Callable):
    def __call__(self) -> dict[str, float]:
        pass

class RandomMasking(MaskingAlgorithm):
    """RandomMasking class. Computes the patch percent drop. A full task is dropped if all patches are dropped"""
    def __init__(self, tasks: list[str], drop_patch_percent: float, drop_task_percent: float,
                 excluded_tasks: list[str] | None = None, included_tasks: list[str] | None = None):
        excluded_tasks = [] if excluded_tasks is None else excluded_tasks
        included_tasks = [] if included_tasks is None else included_tasks
        assert set(included_tasks).intersection(excluded_tasks) == set(), f"{included_tasks=}\n{excluded_tasks=}"
        assert (diff := set(excluded_tasks).difference(tasks)) == set(), f"Unknown tasks provided: {diff} vs {tasks}"
        assert len(excluded_tasks) < len(tasks), "Cannot exclude all nodes"
        assert (drop_patch_percent + drop_task_percent) > 0, "One must be set!"
        self.tasks = tasks
        self.excluded_tasks = excluded_tasks
        self.included_tasks = included_tasks
        self.drop_patch_percent = drop_patch_percent
        self.drop_task_percent = drop_task_percent

    def __call__(self, tries: int = 0) -> dict[str, float]:
        assert tries <= 30, f"Possibly wrong args: {self}"
        drop_percent = {}
        for k in self.tasks:
            if k in self.excluded_tasks:
                drop_percent[k] = 100
            elif k in self.included_tasks:
                drop_percent[k] = 0
            else:
                drop_percent[k] = 100 if random() < self.drop_task_percent / 100 else self.drop_patch_percent
        if all(x == 100 for x in drop_percent.values()):
            return self.__call__(tries + 1)
        return drop_percent

    def __repr__(self) -> str:
        return f"[RandomMasking] Tasks: {self.tasks} (excluded: {self.excluded_tasks}), " \
               f"drop task(%): {self.drop_task_percent}, drop patch(%): {self.drop_patch_percent}"

class FixedMasking(MaskingAlgorithm):
    def __init__(self, combos: list[Combo]):
        assert all(set(combo.input_tasks).intersection(combo.output_tasks) == set() for combo in combos), combos
        assert len(set(combos)) == len(combos), f"We may have duplicate combos: {combos}"
        self.combos = combos

    @staticmethod
    def build_from_tasks(input_tasks: list[str], output_tasks: list[str]) -> FixedMasking:
        """Given it=[a,b], ot=[c,d]. Return: [a->c, a->d, b->c, b->d, a->cd, b->cd, ab->c, ab->d, ab->cd]"""
        all_inputs, all_outputs = powerset(input_tasks), powerset(output_tasks)
        res = [Combo(c[0], c[1]) for c in product(all_inputs, all_outputs)]
        logger.info(f"allowed_combos is ???. Setting to all combinations of input-output. Got {len(res)} combos.")
        return FixedMasking(res)

    def __call__(self) -> dict[str, float]:
        return choice(self.combos).to_dict()

    def __repr__(self):
        return f"[FixedMasking] Combos ({len(self.combos)}): {self.combos}"

class FixedOrderedMasking(FixedMasking):
    """Same as FixedMasking but without random choice. It keeps state of current ix and updates it at __call__"""
    def __init__(self, combos: list[Combo]):
        super().__init__(combos)
        self.ix = 0

    def reset(self):
        """resets the index, must be called by the user (usually at inference-level)"""
        self.ix = 0

    def __call__(self) -> dict[str, float]:
        assert self.ix < len(self.combos), (self.ix, self.combos)
        res = self.combos[self.ix].to_dict()
        self.ix += 1
        return res

    def __repr__(self):
        return f"[FixedOrderedMasking] Combos ({len(self.combos)}): {self.combos}"

def test_RandomMasking_no_excluded():
    tasks = ["A", "B", "C"]
    masks = (masking := RandomMasking(tasks=tasks, drop_patch_percent=50, drop_task_percent=0, excluded_tasks=[]))()
    assert masking.drop_task_percent == 0
    assert all(masks[t] == 50 for t in tasks)

    at_least_one = 0
    for _ in range(10):
        masks = RandomMasking(tasks=tasks, drop_patch_percent=0, drop_task_percent=75, excluded_tasks=[])()
        at_least_one += any(v == 100 for v in masks.values())
    assert at_least_one > 0

    for _ in range(10):
        rm = RandomMasking(tasks=tasks, drop_patch_percent=0, drop_task_percent=50, excluded_tasks=["A"])()
        assert rm["A"] == 100

    for _ in range(10):
        rm = RandomMasking(tasks=tasks, drop_patch_percent=0, drop_task_percent=50, included_tasks=["A"])()
        assert rm["A"] == 0

def test_FixedMasking_build_from_tasks():
    input_tasks, output_tasks = ["a", "b"], ["c", "d"]
    res = FixedMasking.build_from_tasks(input_tasks=input_tasks, output_tasks=output_tasks)
    assert str(res.combos) == ("[[a -> c], [a -> d], [a -> c,d], [b -> c], [b -> d], [b -> c,d],"
                               " [a,b -> c], [a,b -> d], [a,b -> c,d]]")
    for _ in range(10):
        masking = res()
        assert all(k in input_tasks for k, v in masking.items() if v == 0)
        assert all(k in output_tasks for k, v in masking.items() if v == 100)

def test_FixedMasking_manual_list():
    res = FixedMasking([Combo(["a", "b"], ["c", "d"]), Combo(["a"], ["b", "c", "d"])])
    assert str(res.combos) == "[[a,b -> c,d], [a -> b,c,d]]"
    for _ in range(10):
        masking = res()
        assert masking == {"a": 0, "b": 100, "c": 100, "d": 100} or masking == {"a": 0, "b": 0, "c": 100, "d": 100}

def test_FixedOrderedMasking_manual_list():
    res = FixedOrderedMasking([Combo(["a", "b"], ["c", "d"]), Combo(["a"], ["b", "c", "d"]), Combo(["a"], ["d"])])
    assert str(res.combos) == "[[a,b -> c,d], [a -> b,c,d], [a -> d]]"

    for _ in range(3):
        assert (r := res()) == {"a": 0, "b": 0, "c": 100, "d": 100}, r
        assert (r := res()) == {"a": 0, "b": 100, "c": 100, "d": 100}, r
        assert (r := res()) == {"a": 0, "d": 100}, r
        with pytest.raises(AssertionError):
            _ = res()
        res.reset()
    assert res.ix == 0

if __name__ == "__main__":
    test_FixedOrderedMasking_manual_list()
