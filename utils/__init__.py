"""init file"""
import sys
from omegaconf import OmegaConf

from .utils import *
from .patch_tensor import patch_tensor, unpatch_with_size, mask_tensor, fill_masked_with_value
from .positional_encodings_2d import positionalencoding2d
from .metrics import MetricFn, l2, ce, MeanIoU, build_metrics, compute_metrics
from .loss_computer import LossComputer, build_loss_computer
from .masking_algorithm import MaskingAlgorithm, build_masking_algorithm

def oc_args(x: str) -> str:
    """parses ${args:xxx} to match CLI args '--xxx yyy' and returns yyy. Only works for positional args"""
    ix = sys.argv.index(f"--{x}")
    assert (L := len(sys.argv)) == ix + 2 or (L >= ix + 3 and sys.argv[ix + 2].startswith("--")), (x, "\n", sys.argv)
    return sys.argv[ix + 1]

OmegaConf.register_new_resolver("len", len)
OmegaConf.register_new_resolver("str", str)
OmegaConf.register_new_resolver("sorted", sorted)
OmegaConf.register_new_resolver("eq", lambda a, b: a == b)
OmegaConf.register_new_resolver("pretrained", lambda x: x)
OmegaConf.register_new_resolver("dict", dict)
OmegaConf.register_new_resolver("flatten_unique", lambda x: sorted(set(flatten_list(x))))
OmegaConf.register_new_resolver("args", oc_args)
OmegaConf.register_new_resolver("values", lambda x: x.values())
