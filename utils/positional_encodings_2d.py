"""https://github.com/wzlxjtu/PositionalEncoding2D/blob/master/positionalembedding2d.py"""
import math
import torch as tr

def positionalencoding2d(d_model: int, height: int, width: int):
    """
    :param d_model: dimension of the model
    :param height: height of the positions
    :param width: width of the positions
    :return: d_model*height*width position matrix
    """
    if d_model % 4 != 0:
        raise ValueError("Cannot use sin/cos positional encoding with "
                         "odd dimension (got dim={:d})".format(d_model))
    pe = tr.zeros(d_model, height, width)
    # Each dimension use half of d_model
    d_model = int(d_model / 2)
    div_term = tr.exp(tr.arange(0.0, d_model, 2) * -(math.log(10000) / d_model))
    pos_w = tr.arange(0.0, width).unsqueeze(1)
    pos_h = tr.arange(0.0, height).unsqueeze(1)
    pe[0:d_model:2, :, :] = tr.sin(pos_w * div_term).transpose(0, 1).unsqueeze(1).repeat(1, height, 1)
    pe[1:d_model:2, :, :] = tr.cos(pos_w * div_term).transpose(0, 1).unsqueeze(1).repeat(1, height, 1)
    pe[d_model::2, :, :] = tr.sin(pos_h * div_term).transpose(0, 1).unsqueeze(2).repeat(1, 1, width)
    pe[d_model + 1::2, :, :] = tr.cos(pos_h * div_term).transpose(0, 1).unsqueeze(2).repeat(1, 1, width)

    return pe.permute(1, 2, 0) # [H, W, E]
