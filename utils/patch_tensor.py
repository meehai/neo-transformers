"""Patch/Unpatch/Mask images and tokens."""
from __future__ import annotations
from math import prod
import torch as tr
import pytest
from lightning_module_enhanced.utils import to_device

from lovely_tensors import monkey_patch
monkey_patch()

def unpatch_with_size(patched_tensor: tr.Tensor, original_shape: tuple[int, ...]) -> tr.Tensor:
    C, h, w, (ph, pw) = original_shape[0:-2], *original_shape[-2:], patched_tensor.shape[-3: -1]
    pt1 = patched_tensor.reshape(prod(C), ph, pw, h//ph, w//pw) # (prod(C), ph, pw, h//ph, w//pw)
    unpatched_tensor = pt1.permute(0, 3, 1, 4, 2).reshape(*C, h, w) # -> (prod(C), ph, h//ph, pw, w//pw) -> (*C, H, W)
    return unpatched_tensor

def patch_tensor(tensor: tr.Tensor, patch_size: tuple[int, int]) -> tr.Tensor:
    """Given a regular tensor, patch it and add a few special attributes"""
    C, h, w, ph, pw = tensor.shape[0:-2], *tensor.shape[-2:], *patch_size
    assert h % ph == 0 and w % pw == 0, (h, ph, w, pw)
    tensor_v = tensor.reshape(prod(C), h, w) # (*C, h, w) -> (n=prod(C), h, w)
    tensor_v2 = tensor_v.reshape(prod(C), h//ph, ph, w//pw, pw).permute(0, 2, 4, 1, 3) # (n, ph, pw, h//ph, w//pw)
    patched_tensor = tensor_v2.reshape(*C, ph, pw, -1) # (*C, p_h, p_w, n_tokens)
    return patched_tensor

def mask_tensor(patched_tensor: tr.Tensor, drop_percent: float) -> tuple[tr.Tensor, tr.Tensor]:
    """returns the masked tensor and the applied mask of positions"""
    mask = tr.rand(patched_tensor.shape[-1]).to(patched_tensor.device) <= (drop_percent / 100)
    masked_tensor = patched_tensor[..., ~mask]
    return masked_tensor, mask

def split_and_patch_pred(pred: tr.Tensor, patch_size: tuple[int, int], task_ch: dict[str, int]) -> dict[str, tr.Tensor]:
    """Given a prediction tensor and a dict: task->n_channels, split the prediction into that. Also patch it."""
    n_so_far, res = 0, {}
    assert len(pred.shape) == 4 and (A := pred.shape[1]) == (B := sum(task_ch.values())), (A, B) # (B, sum(ch), H, W)
    y_decoder_patched = patch_tensor(pred, patch_size)
    for task, ch in task_ch.items():
        res[task] = y_decoder_patched[:, n_so_far: n_so_far + ch].permute(0, 4, 1, 2, 3)
        n_so_far += ch
    return res

def fill_masked_with_value(masked_tensor: tr.Tensor, mask: tr.Tensor, value: float) -> tr.Tensor:
    """Given a masked tensor, fill the masked positions with a default value based on the original shape"""
    res = tr.full((*masked_tensor.shape[0:-1], len(mask)), value, dtype=masked_tensor.dtype).to(masked_tensor.device)
    res[..., ~mask] = masked_tensor
    return res

def test_patch_unpatch_tensor():
    B, t, C, H, W = 13, 9, 3, 480, 640
    img = tr.randn(B, t, C, H, W)
    assert not (hasattr(img, "patch_size") or hasattr(img, "original_shape"))
    with pytest.raises(AssertionError):
        patch_tensor(img, (13, 320))
    pt1 = patch_tensor(img, ps := (20, 20))
    assert pt1.shape == (B, t, C, 20, 20, 480 // 20 * 640 // 20) and pt1.shape[-3:-1] == ps
    pt2 = unpatch_with_size(pt1, img.shape)
    assert pt2.shape == (B, t, C, H, W)
    assert tr.allclose(pt2, img)
    pt3 = patch_tensor(pt2, ps := (30, 20))
    assert pt3.shape == (B, t, C, 30, 20, 480 // 30 * 640 // 20) and pt3.shape[-3:-1] == ps
    pt4 = unpatch_with_size(pt3, img.shape)
    assert tr.allclose(pt4, img)
    assert pt4.shape == (B, t, C, H, W)

def test_patch_tensor_to_device():
    B, t, C, H, W = 13, 9, 3, 480, 640
    tasks = ["A", "B", "C"]
    img = {k: tr.randn(B, t, C, H, W) for k in tasks}
    pt = {k: patch_tensor(img[k], (20, 20)) for k in tasks}
    assert all(v.shape == (B, t, C, 20, 20, 480 // 20 * 640 // 20) and v.shape[-3:-1] == (20, 20) for v in pt.values())
    pt2 = to_device(pt, "cpu:1")
    pt3 = {k: unpatch_with_size(v, img[k].shape) for k, v in pt2.items()}
    assert all(v.shape == (B, t, C, H, W) for v in pt3.values())

def test_unpatch_with_size():
    B, t, C, ph, pw, n_patch = 13, 9, 3, 30, 20, 480 // 30 * 640 // 20
    patched_tensor = tr.randn(B, t, C, ph, pw, n_patch)
    assert unpatch_with_size(patched_tensor, (B, t, C, 480, 640)).shape == (B, t, C, 480, 640)

def test_stack():
    B, t, C, H, W = 13, 9, 3, 480, 640
    img = tr.randn(B, t, C, H, W)
    pt1 = patch_tensor(img, (20, 20))
    pt2 = patch_tensor(img, (20, 20))
    pt_stacked = tr.stack([pt1, pt2])
    assert pt_stacked.shape == (2, B, t, C, 20, 20, 480 // 20 * 640 // 20)
    assert unpatch_with_size(pt_stacked, (2, *img.shape)).shape == (2, B, t, C, H, W)

def test_split_and_patch_pred():
    task_ch = {"A": 1, "B": 3, "C": 2}
    B, h, w, ph, pw = 5, 540, 1080, 60, 60
    y_decoder = tr.randn(B, sum(task_ch.values()), h, w)
    with pytest.raises(AssertionError):
        _ = split_and_patch_pred(tr.randn(B, sum(task_ch.values()) - 1, h, w), (ph, pw), task_ch)
        _ = split_and_patch_pred(tr.randn(B, sum(task_ch.values()) + 1, h, w), (ph, pw), task_ch)
    y_decoder_split = split_and_patch_pred(y_decoder, (ph, pw), task_ch)
    assert y_decoder_split.keys() == task_ch.keys()
    for task, item in y_decoder_split.items():
        assert item.shape == (B, h // ph * w // pw, task_ch[task], ph, pw)

def test_fill_masked_with_value():
    C, H, W = 3, 480, 640
    x = tr.randn(1, C, H, W)

    x_patched = patch_tensor(x, patch_size=(20, 20))
    x_masked, mask = mask_tensor(x_patched, drop_percent=50)
    x_patched_filled = fill_masked_with_value(x_masked, mask, 0.5)
    x_filled = unpatch_with_size(x_patched_filled, x.shape)
    assert (x_filled == 0.5).sum() // 3 == mask.sum() * 20 * 20
