"""MetricsComputer implementation. Support for higher level metrics (i.e. moving average/wmean) for neo-transformer."""
from __future__ import annotations
from typing import Any
from loggez import loggez_logger as logger
from omegaconf import DictConfig
import torch as tr
from .utils import MovingAverage

def build_loss_computer(loss_cfg: DictConfig, metrics_list: list[str]) -> LossComputer:
    """builds the loss computer as a weighted average basd on the metrics list and some potential overwrites"""
    if loss_cfg.loss_overwrites is not None:
        for task, task_metrics in loss_cfg.loss_overwrites.items():
            current = [metric for metric in metrics_list if metric.startswith(f"{task}_")]
            new = [f"{task}_{metric_name}" for metric_name in task_metrics]
            assert all(_x in metrics_list for _x in new), (new, metrics_list)
            metrics_list = list(set(metrics_list) - set(current)) + new
            logger.info(f"Updating the loss metrics of '{task}' from {current} to {new}")
    return LossComputer(metrics_list, **{k: v for k, v in loss_cfg.items() if k != "loss_overwrites"})

class LossComputer:
    """LossComputer. Computes loss as a (weighted) mean from the computed metrics via self.__call__(metrics)"""
    def __init__(self, metric_names: list[str], loss_type: str):
        assert len(metric_names) > 0 and all(isinstance(x, str) for x in metric_names), metric_names
        assert len(set(metric_names)) == len(metric_names), f"Duplicates in metric names: {metric_names}"
        assert loss_type in ("average", "adaptive", "weighted_average"), self.loss_type
        self.loss_type = loss_type
        self.metric_names = list(metric_names)
        self.moving_averages = {name: MovingAverage() for name in metric_names} if loss_type == "adaptive" else None

    def __call__(self, metrics: dict[str, tr.Tensor]) -> tr.Tensor | None:
        """Given the output of compute_metrics, compute the loss."""
        metrics_filtered = {k: v for k, v in metrics.items() if v is not None and k in self.metric_names}
        if len(metrics_filtered) == 0:
            logger.debug(f"Got 0 items to compute loss from. {metrics=}")
            return None

        if self.loss_type == "average":
            res = sum(metrics_filtered.values()) / len(metrics_filtered)

        if self.loss_type == "adaptive":
            items = []
            for k, score in metrics_filtered.items():
                items.append(score / (self.moving_averages[k].update(score.detach()) + 1e-5))
            res = sum(items) / len(items) # simple average since all the losses are not weighted anymore here

        if self.loss_type == "weighted_average":
            raise NotImplementedError

        return res

    def __str__(self):
        return f"[LossComputer] Metrics ({len(self.metric_names)}): {self.metric_names}. Type: {self.loss_type}"

    def __repr__(self):
        return str(self)

def test_LossComputer():
    tasks = ["task1", "task2"]
    MB, H, W, chs = 10, 20, 30, {"task1": 1, "task2": 3}
    metric_fns = {
        "task1_l1": lambda y, gt, **k: (y - gt).abs().mean(),
        "task1_l2": lambda y, gt, **k: (y - gt).pow(2).mean(),
        "task2_l1_5": lambda y, gt, **k: (y - gt).abs().mean() + 5,
        "task2_l2": lambda y, gt, **k: (y - gt).pow(2).mean()
    }
    y = {task: tr.randn(MB, H, W, chs[task]) for task in tasks}
    gt = {task: tr.randn(MB, H, W, chs[task]) for task in tasks}
    loss_computer = LossComputer(metric_fns, loss_type="average")
    assert set(loss_computer.metric_names) == {"task2_l2", "task1_l2", "task1_l1", "task2_l1_5"}
    metrics = {}
    for task in tasks:
        for metric_name in [k for k in metric_fns if k.startswith(f"{task}_")]:
            metrics[metric_name] = metric_fns[metric_name](y[task], gt[task])
    loss = loss_computer(metrics)
    assert isinstance(loss, tr.Tensor) and isinstance(loss.item(), float)

    loss_computer = LossComputer(["task2_l2", "task1_l1", "task2_l1_5"], loss_type="adaptive")
    loss2 = loss_computer(metrics)
    assert isinstance(loss, tr.Tensor) and isinstance(loss.item(), float)
    assert loss != loss2, (loss, loss2)
