#!/usr/bin/env python3
"""given a directory of -o_ens_npz output from inference.py and a directory of gt, get top k best ensembles and eval"""
from argparse import ArgumentParser, Namespace
from pathlib import Path
from tqdm import trange, tqdm
from natsort import natsorted
import torch as tr
import numpy as np
# from contexttimer import Timer
from pprint import pprint
from torchmetrics.functional.classification import multiclass_stat_scores
from multiprocessing.pool import ThreadPool

def mean(x):
    return sum(x) / len(x)

def get_ious_per_scene(ious: tr.Tensor, names: list[str], scenes: list[str]) -> tuple[tr.Tensor, ...]:
    res = []
    for scene in scenes:
        ixs = [names.index(x) for x in names if x.startswith(scene)]
        res += [ [ious[ix] for ix in ixs] ]
    return res

def wmiou_from_pred(y: tr.Tensor, gt: tr.Tensor, weights: tr.Tensor) -> float:
    assert y.dtype == tr.int64 and gt.dtype == tr.int64, (y.dtype, gt.dtype)
    assert tr.allclose(weights.sum(), tr.FloatTensor([1])), (weights, weights.sum())
    res = multiclass_stat_scores(y, gt, num_classes=len(weights), average=None)
    tp, fp, _, fn = res.T[0:4]
    iou = tp / (tp + fp + fn)
    iou[tr.isnan(iou)] = 0
    wmiou = (iou * weights).sum()
    return wmiou

def load_all_files(pred_dir: Path, gt_dir: Path, n_threads: int=16) -> tuple[list[np.ndarray], list[np.ndarray]]:
    pred_names = natsorted([x.name for x in pred_dir.iterdir() if x.is_dir()])
    gts, preds = [], []
    for i in trange(len(pred_names), desc="load files"):
        all_pred_files = list((pred_dir / pred_names[i]).iterdir())
        # with Timer(prefix=f"loading {pred_names[i]=} {len(all_pred_files)=} threads={n_threads}"):
        # preds = [np.load(x)["arr_0"] for x in all_pred_files][0:10]
        with ThreadPool(max(n_threads, len(all_pred_files))) as pool:
            preds += [pool.map(lambda p: np.load(p)["arr_0"], all_pred_files)]

        gts += [np.load(gt_dir / pred_names[i])["arr_0"]]
    return preds, gts

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("pred_dir", type=Path)
    parser.add_argument("gt_dir", type=Path)
    parser.add_argument("--top_k", type=int, default=10)
    parser.add_argument("--classes", nargs="+")
    parser.add_argument("--class_weights", nargs="+", type=float)
    parser.add_argument("--scenes", nargs="+", help="each scene will get separate metrics if provided")
    args = parser.parse_args()
    if args.classes is None:
        args.classes = ["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"]
    if args.class_weights is None:
        args.class_weights = [0.28172092, 0.30589653, 0.13341699, 0.05937348,
                              0.00474491, 0.05987466, 0.08660721, 0.06836531]
    if args.scenes is None:
        args.scenes = ["barsana", "comana", "norway"]
    return args

def main(args: Namespace):
    """main fn"""
    gt_names = [x.name for x in args.gt_dir.iterdir()]
    pred_names = natsorted([x.name for x in args.pred_dir.iterdir() if x.is_dir()])
    assert all(p in gt_names for p in pred_names)
    n_threads = 32
    K = 100 # number of individual predictions... we can assert this is true based on # of npz files
    # preds :: (N, K, H, W, C), gts :: (N, H, W, C)
    preds, gts = load_all_files(args.pred_dir, args.gt_dir, n_threads=n_threads)
    n = len(preds)
    weights = tr.FloatTensor(args.class_weights)
    
    # eval individual preds
    if not (res_path := Path(f"{args.pred_dir}/.results.pkl")).exists():
        ious = tr.zeros(n, K) - 10_000
        for i, (pred, gt) in tqdm(enumerate(zip(preds, gts)), desc="eval individual preds", total=len(preds)):
            this_ious = tr.zeros(len(pred))
            # with Timer(prefix="evaluation predictions"):
            for j, pred_j in enumerate(pred):
                this_ious[j] = wmiou_from_pred(y=tr.from_numpy(pred_j.argmax(-1)),
                                               gt=tr.from_numpy(gt).long(), weights=weights)
            ious[i, 0:len(this_ious)] = this_ious
        tr.save(ious, res_path)
    else:
        ious = tr.load(res_path)
        assert not (ious < 0).any(), (res_path, "\n", ious)

    ious_per_scene = []
    for scene in args.scenes: ious_per_scene += [ ious[[pred_names.index(x) for x in pred_names if x.startswith(scene)]] ]
    avg, std = [x.mean() * 100 for x in ious_per_scene], [x.std(1).mean() * 100 for x in ious_per_scene]
    print(f"avg: {round(mean(avg).item(), 3)} +/- {round(mean(std).item(), 3)}")

    # compute top-k best based on individual
    ks = [1, 2, 3, 5, 10, 20, 30, 50]
    best_ious = tr.zeros(n, len(ks)) # ens=1, 2, 3, 5, 10, 20, 30, 50
    for i, (pred, gt) in tqdm(enumerate(zip(preds, gts)), desc="eval ensembles", total=len(preds)):
        inds = ious[i].sort(descending=True).indices
        for j, k in enumerate(ks):
            topk_vals = np.array([pred[_ix] for _ix in inds[0:k]]) # (N, K, H, W, C)
            topk_pred = tr.from_numpy(topk_vals.mean(0))
            best_ious[i, j] = wmiou_from_pred(y=topk_pred.argmax(-1), gt=tr.from_numpy(gt).long(), weights=weights)

    ious_per_scene = []
    for scene in args.scenes: ious_per_scene += [ best_ious[[pred_names.index(x) for x in pred_names if x.startswith(scene)]] ]
    avg = mean([x.mean(0)*100 for x in ious_per_scene])
    pprint(dict(zip(ks, [round(x.item(), 3) for x in avg])))

    # compute random k based on individual sampling s=20 times for each k
    s = 20
    k_ious = tr.zeros(n, s, len(ks)) # ens=1, 2, 3, 5, 10, 20, 30, 50
    for i, (pred, gt) in tqdm(enumerate(zip(preds, gts)), desc="eval ensembles", total=len(preds)):
        for _s in range(s):
            for j, k in enumerate(ks):
                ixs = np.random.choice(K, size=k, replace=False)
                k_vals = np.array([pred[_ix] for _ix in ixs]) # (N, K, H, W, C)
                k_pred = tr.from_numpy(k_vals.mean(0))
                k_ious[i, _s, j] = wmiou_from_pred(y=k_pred.argmax(-1), gt=tr.from_numpy(gt).long(), weights=weights)
    ious_per_scene = []
    for scene in args.scenes: ious_per_scene += [ k_ious[[pred_names.index(x) for x in pred_names if x.startswith(scene)]] ]
    avgs = mean([_i.mean(1).mean(0)*100 for _i in ious_per_scene])
    stds = mean([_i.std(1).mean(0)*100 for _i in ious_per_scene])
    pprint(dict(zip(ks, [f"{round(x.item(), 3)}+/-{round(y.item(), 3)}" for x, y in zip(avgs, stds)])))

if __name__ == "__main__":
    main(get_args())
