#!/usr/bin/bash
set -ex
CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

if [ "$(hostname)" == "sprmcrogpu-wn13.grid.pub.ro" ]; then
    DATASET_DIR="/export/home/proiecte/aux/mihai_cristian.pirvu/datasets/dronescapes-2024"
elif [ "$(hostname)" == "dgxa100-ncit-wn01.grid.pub.ro" ]; then
    DATASET_DIR="/tmp/dronescapes_tmp"
else
    test "$#" == "3" || exit 1
fi

find $1 -name "*.ckpt" | sort -m | while read line; do
    echo "----"
    echo "line = $line";
    name=$(echo $line | rev | cut -d "/" -f 1 | rev)
    epoch=$(echo $name | cut -d "/" -f 2 | cut -d "-" -f1);
    version=$(echo $line | rev | cut -d "/" -f 2 | rev) # dir name
    echo "name = $name"
    echo "epoch = $epoch"
    echo "version = $version"

    if [ ${version:0:3} == "mae" ]; then
        enss=(1 2 3 5 10 20 30 50)
    else
        enss=(1)
    fi

    if [ ${version:0:2} == "sl" ]; then
        exported_tasks=("semantic_output")
    else
        exported_tasks=("semantic_output" "depth_output" "camera_normals_output")
    fi


    for ens in ${enss[@]}; do
        out_dir="$(dirname $line)"/"$version"_"$epoch"_"$ens"
        if [ -d "$out_dir" ]; then
            echo "$out_dir" exists
        else
            $CWD/../inference.py \
                "$DATASET_DIR"/data/test_set_annotated_only/ \
                $line \
                --replace_cfg data.loader_params.batch_size 1 \
                -o_npz "$out_dir" \
                --exported_tasks "${exported_tasks[@]}" \
                --n_ensembles $ens;
        fi
        $CWD/eval.sh \
            "$out_dir" \
            "$DATASET_DIR"/data/test_set_annotated_only/ \
            "$DATASET_DIR"/data/train_set/.task_statistics.npz
    done
done
