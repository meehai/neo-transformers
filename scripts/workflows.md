# Useful commands for these scripts

### Evaluate one inference on all 3 tasks

First, run inference
```bash
CUDA_VISIBLE_DEVICES=7 ./inference.py ~/datasets/dronescapes-2024/data/test_set_annotated_only/ ckpts/safeuav/sema/mae-4M-ext/epoch\=37-val_semantic_output_mean_iou\=0.470.ckpt --replace_cfg data.loader_params.batch_size 1 -o_npz out_dir --n_ensembles 1 --exported_tasks semantic_output depth_output camera_normals_output;```
```

Then, run the `eval.sh` script

```bash
bash eval.sh out_dir  ~/datasets/dronescapes-2024/data/test_set_annotated_only/
```

### Ensemble and evaluate a directory of predictions

First, get a bunch of checkpoints in a dir of dirs:
```bash
find ckpts/safeuav/sema/ -name "*.ckpt"
ckpts/safeuav/sema/mae-4M-ext/epoch=37-val_semantic_output_mean_iou=0.470.ckpt
ckpts/safeuav/sema/mae-4M-train/epoch=1-val_loss=0.456.ckpt
ckpts/safeuav/sema/mae-10M-ext/epoch=23-val_semantic_output_mean_iou=0.448.ckpt
```

Then, run the `ensemble_and_eval.sh` script that does `inference.py` and `eval.sh` for a list of ensembles (1 2 5 ... 50)

```
CUDA_VISIBLE_DEVICES=7 ./ensemble_and_eval.sh ../ckpts/safeuav/sema/
```

Will result in

```bash
ls ckpts/safeuav/sema/*
ckpts/safeuav/sema/mae-10M-ext:
'epoch=23-val_semantic_output_mean_iou=0.448.ckpt'  'mae-10M-ext_epoch=23_2'   'mae-10M-ext_epoch=23_30'
'mae-10M-ext_epoch=23_1'                            'mae-10M-ext_epoch=23_20'  'mae-10M-ext_epoch=23_5'
'mae-10M-ext_epoch=23_10'                           'mae-10M-ext_epoch=23_3'   'mae-10M-ext_epoch=23_50'

ckpts/safeuav/sema/mae-4M-ext:
'epoch=37-val_semantic_output_mean_iou=0.470.ckpt'  'mae-4M-ext_epoch=37_2'   'mae-4M-ext_epoch=37_5'
'mae-4M-ext_epoch=37_1'                             'mae-4M-ext_epoch=37_20'  'mae-4M-ext_epoch=37_50'
'mae-4M-ext_epoch=37_10'                            'mae-4M-ext_epoch=37_3'    'mae-4M-ext_epoch=37_30'

ckpts/safeuav/sema/mae-4M-train:
'epoch=1-val_loss=0.456.ckpt'  'mae-4M-train_epoch=1_2'   'mae-4M-train_epoch=1_30'
'mae-4M-train_epoch=1_1'       'mae-4M-train_epoch=1_20'  'mae-4M-train_epoch=1_5'
'mae-4M-train_epoch=1_10'      'mae-4M-train_epoch=1_3'   'mae-4M-train_epoch=1_50'
```

You can have >1 .ckpt files in teh directory as well. Existing results are skipped (but not checked if all data is
there, so delete partial results manually)

### Ensemble: extract top 100, get results for top 2, 5, etc (with GT)

First, extract the 100 ensembles

```bash
CUDA_VISIBLE_DEVICES=7 ./inference.py ~/datasets/dronescapes-2024/data/test_set_annotated_only/ ckpts/safeuav/sema/mae-4M-ext/epoch\=37-val_semantic_output_mean_iou\=0.470.ckpt --replace_cfg data.loader_params.batch_size 1 -o_ens_npz ckpts/safeuav/sema/mae-4M-ext/mae-4M-ext_epoch\=37_100_all --n_ensembles 100 --exported_tasks semantic_output;
```

followed by

```bash
./evaluate_ens_topk.py ../../ckpts/safeuav/sema/mae-4M-ext/mae-4M-ext_epoch\=37_100_all/semantic_output/ ~/datasets/dronescapes-2024/data/test_set_annotated_only/semantic_output/
```
