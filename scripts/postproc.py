#!/usr/bin/env python3
"""post-processing functions used to improve the predictions at test time using various strategies"""
import numpy as np
from argparse import Namespace, ArgumentParser
from pathlib import Path
from tqdm import tqdm
from multiprocessing import Pool
from natsort import natsorted
from functools import partial
from loggez import loggez_logger as logger
import shutil

def ix(cls: str, classes: list[str]):
    return classes.index(cls)

def oh(cls: str, classes: list[str]):
    return np.eye(len(classes))[ix(cls, classes)].astype(int)

def apply_pos(sema: np.ndarray, binary_map: np.ndarray, mapped_list: list[str], classes: list[str]) -> np.ndarray:
    assert isinstance(mapped_list, list), type(mapped_list)
    assert all(cls in classes for cls in mapped_list), (mapped_list, classes)
    if binary_map is None:
        print("binary map is None, returning as-is")
        return sema
    res = sema.copy()
    cond = np.where(binary_map >= 0.5)
    XX = res[cond]
    YY = XX[:, [ix(cls, classes) for cls in mapped_list]].argmax(-1)
    YY_oh = np.array([oh(cls, classes) for cls in mapped_list])
    res[cond] = YY_oh[YY]
    # print(mapped_list, "=>", round(100 - (sema.argmax(-1) == res.argmax(-1)).mean() * 100, 2), "%")
    return res

def apply_neg(sema: np.ndarray, binary_map: np.ndarray, mapped_list: list[str], classes: list[str]) -> np.ndarray:
    assert isinstance(mapped_list, list), type(mapped_list)
    assert all(cls in classes for cls in mapped_list), (mapped_list, classes)
    if binary_map is None:
        print("binary map is None, returning as-is")
        return sema
    not_classes = [_y for _y in classes if _y not in mapped_list]
    res = sema.copy()
    cond = np.where(binary_map <= -0.5)
    XX = res[cond]
    YY = XX[:, [ix(cls, classes) for cls in not_classes]].argmax(-1)
    YY_oh = np.array([oh(cls, classes) for cls in not_classes])
    res[cond] = YY_oh[YY]
    # print(mapped_list, "=>", round(100 - (sema.argmax(-1) == res.argmax(-1)).mean() * 100, 2), "%")
    return res

def apply_postproc(sema: np.ndarray, buildings: np.ndarray, vegetation: np.ndarray, transportation: np.ndarray,
                   sky_and_water: np.ndarray, safe_landing_semantics: np.ndarray, containing: np.ndarray,
                   classes: list[str], return_all: bool = False) -> np.ndarray | list[np.ndarray]:
    sema1 = apply_pos(sema, buildings, ["residential"], classes)
    sema2 = apply_pos(sema1, vegetation, ["hill", "forest", "land"], classes)
    sema3 = sema2
    if transportation is not None and containing is not None:
        sema3 = apply_pos(sema2, transportation * (containing >= 0.5), ["road"], classes)
    if transportation is not None and containing is not None:
        sema3 = apply_pos(sema3, transportation * (containing < 0.5), ["little-objects"], classes)
    sema4 = sema3
    if sky_and_water is not None and containing is not None:
        sema4 = apply_pos(sema3, sky_and_water * (containing >= 0.5), ["water"], classes)
    if sky_and_water is not None and containing is not None:
        sema4 = apply_pos(sema4, sky_and_water * (containing < 0.5), ["sky"], classes)
    sema5 = apply_neg(sema4, safe_landing_semantics, ["land"], classes)
    return sema5 if not return_all else [sema1, sema2, sema3, sema4, sema5]

def do_one(item: list[np.ndarray], classes: list[str]):
    sema, b, veg, sw, t, c, sls = [np.load(_item)["arr_0"].squeeze() for _item in item[0:-1]]
    sema_postproc_oh = apply_postproc(sema, b, veg, t, sw, sls, c, classes, return_all=False)
    np.savez_compressed(item[-1], sema_postproc_oh)

def main(args: Namespace):
    files = list(args.pred_dir.iterdir())
    assert len(files) > 0, f"No files found at '{args.pred_dir}'"
    assert (args.pred_dir / args.semantic_task_name).exists(), files
    for t in args.binary_task_names:
        assert (args.pred_dir / t).exists(), f"'{t}' not in {files}"
    paths = [natsorted((args.pred_dir / k).iterdir(), key=lambda p: p.name)
             for k in [args.semantic_task_name, *args.binary_task_names]]
    paths = np.array(paths).T
    assert len(paths) > 0, len(paths)
    out_paths = np.array([args.output_dir / "semantic_segprop8" / item[0].name for item in paths])[..., None]
    paths = np.concatenate([paths, out_paths], axis=1)
    logger.info(f"Found {len(paths)} to make post-processing from")
    shutil.rmtree(args.output_dir, ignore_errors=True)
    (args.output_dir / "semantic_segprop8").mkdir(parents=True)

    classes_8 = ["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"]
    map_fn = map if args.n_workers == 1 else Pool(args.n_workers).imap
    fn = partial(do_one, classes=classes_8)
    list(tqdm(map_fn(fn, paths), total=len(paths)))
    logger.info(f"Stored results at '{args.output_dir}'")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("pred_dir", type=Path)
    parser.add_argument("--output_dir", "-o", type=Path, required=True)
    parser.add_argument("--semantic_task_name", default="semantic_segprop8")
    parser.add_argument("--binary_task_names", nargs="+",
                        default=["buildings", "vegetation", "sky-and-water",
                                 "transportation", "containing", "safe-landing-semantics"])
    parser.add_argument("--overwrite", action="store_true")
    parser.add_argument("--n_workers", type=int, default=1)
    args = parser.parse_args()
    assert not args.output_dir.exists() or args.overwrite, f"'{args.output_dir}' exists. Use --overwrite."
    main(args)
