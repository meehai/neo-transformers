#!/usr/bin/bash
set -ex
CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

test -d "$1" || ( echo pred dir "$1" does not exist; kill $$ )
test -d "$2" || ( echo gt dir "$2" does not exist; kill $$ )
test -f "$3" || ( echo statistics file "$3" does not exist; kill $$ )

function eval {
    test -f "$1"/res.csv || \
        $CWD/evaluate_semantic_segmentation.py \
            "$1"/semantic_output \
            "$2"/semantic_output \
            -o "$1"/res.csv \
            --classes land forest residential road little-objects water sky hill \
            --class_weights 0.28172092 0.30589653 0.13341699 0.05937348 0.00474491 0.05987466 0.08660721 0.06836531 \
            --scenes barsana_DJI_0500_0501_combined_sliced_2700_14700 comana_DJI_0881_full norway_210821_DJI_0015_full \
            --n_workers 8

    if [[ -d "$1"/depth_output ]]; then # some models only output semantic
        test -f "$1"/res_depth.csv || \
            $CWD/evaluate_regression.py \
                "$1"/depth_output \
                "$2"/depth_output \
                -o "$1"/res_depth.csv \
                --scenes barsana_DJI_0500_0501_combined_sliced_2700_14700 \
                --statistics_file $3 \
                --max_depth_overwrite 400
    fi

    if [[ -d "$1"/camera_normals_output ]]; then  # some models only output semantic
        test -f "$1"/res_camera_normals.csv || \
            $CWD/evaluate_regression.py \
                "$1"/camera_normals_output \
                "$2"/camera_normals_output \
                -o "$1"/res_camera_normals.csv \
                --scenes barsana_DJI_0500_0501_combined_sliced_2700_14700 \
                --statistics_file $3
    fi
}

eval "$1" "$2" "$3"
