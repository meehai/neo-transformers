#!/usr/bin/env python3
"""
Evaluation script for regression tasks for dronescapes. Outputs L1*100 for the classes and each frame.
Usage: ./evaluate_regression.py y_dir gt_dir -o results.csv
"""
import os
from loggez import loggez_logger as logger
from pathlib import Path
from argparse import ArgumentParser, Namespace
from tempfile import TemporaryDirectory
from multiprocessing import Pool
from functools import partial
from torchmetrics.functional.classification import multiclass_stat_scores
from tqdm import tqdm
from vre.readers import MultiTaskDataset
from vre_repository.depth import DepthRepresentation
from vre_repository.normals import NormalsRepresentation
import torch as tr
import numpy as np
import pandas as pd

def _check_and_symlink_dirs(y_dir: Path, gt_dir: Path) -> Path:
    """checks whether the two provided paths are actual full of npz directories and links them together in a tmp dir"""
    pred_names, gt_names = {x.name for x in y_dir.iterdir()}, {x.name for x in gt_dir.iterdir()}
    assert all(x.endswith(".npz") for x in [*pred_names, *gt_names]), \
        f"Not dirs of only .npz files: {pred_names} \n {gt_names}"
    # gt files (sfm) are less on dronescapes than all the possible predictions on all 3 scenes -_-
    assert all(x in pred_names for x in gt_names), f"{pred_names} \n vs \n {gt_names}"
    (temp_dir := Path(TemporaryDirectory().name)).mkdir(exist_ok=False)
    os.symlink(gt_dir, temp_dir / "gt")
    os.mkdir(temp_dir / "pred")
    for name in gt_names:
        os.symlink(y_dir / name, temp_dir / "pred" / name)
    return temp_dir

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("y_dir", type=lambda p: Path(p).absolute())
    parser.add_argument("gt_dir", type=lambda p: Path(p).absolute())
    parser.add_argument("--statistics_file", type=Path, required=True)
    parser.add_argument("--output_path", "-o", type=Path, required=True)
    parser.add_argument("--scenes", nargs="+", default=["all"], help="each scene will get separate metrics if provided")
    parser.add_argument("--max_depth_overwrite", type=float, help="Sometimes the global max is different...")
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    assert args.output_path.suffix == ".csv", f"Prediction file must end in .csv, got: '{args.output_path.suffix}'"
    if len(args.scenes) > 0:
        logger.info(f"Scenes: {args.scenes}")
    assert len(args.scenes) == 1, "Sanity check for dronescapes"
    if args.output_path.exists() and args.overwrite:
        os.remove(args.output_path)
    return args

def main(args: Namespace):
    # setup to put both directories in the same parent directory for the reader to work.
    stats = np.load(args.statistics_file, allow_pickle=True)["arr_0"].item()
    temp_dir = _check_and_symlink_dirs(args.y_dir, args.gt_dir)
    _min, _max, mean, std = [x.numpy() for x in stats[args.y_dir.name]]
    if args.y_dir.name.startswith("depth"):
        pred_repr = DepthRepresentation("pred", min_depth=_min, max_depth=_max)
        gt_repr = DepthRepresentation("gt", min_depth=_min, max_depth=_max)
        if args.max_depth_overwrite is not None:
            logger.info(f"Depth is overwritten from the CLI: {_max} to {args.max_depth_overwrite}")
            _max = args.max_depth_overwrite
    else:
        pred_repr = NormalsRepresentation("pred")
        gt_repr = NormalsRepresentation("gt")
    reader = MultiTaskDataset(temp_dir, task_names=["pred", "gt"], task_types={"pred": pred_repr, "gt": gt_repr},
                              handle_missing_data="drop", normalization=None)
    assert (a := len(reader.files_per_repr["gt"])) == (b := len(reader.files_per_repr["pred"])), f"{a} vs {b}"

    res = []
    for item in tqdm(reader):
        gt = item[0]["gt"]
        pred = item[0]["pred"] * std + mean
        M = gt.sum(-1) == 0
        l1_score = (pred[~M] - gt[~M]).abs().mean().item()
        res.append((item[1], l1_score))
    args.output_path.parent.mkdir(exist_ok=True, parents=True)
    logger.info(f"Stored raw metrics file to: '{args.output_path}'")
    (df := pd.DataFrame(res, columns=["name" ,"l1"]).set_index("name")).to_csv(args.output_path)

    # 1 scene only for dronescapes test
    df_agg = df.mean() * 100
    if args.y_dir.name.startswith("depth"):
        df_agg /= _max
    df_agg.round(3).to_csv(args.output_path.parent / f"{args.output_path.stem}_agg.csv")
    print(df_agg)

if __name__ == "__main__":
    main(get_args())
