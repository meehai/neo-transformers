#!/usr/bin/bash
set -ex
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

dataset=$1
echo "Chosen dataset: $dataset"

if ! [ $dataset == "neo" -o $dataset == "dronescapes" ]; then
    echo "Wrong dataset: $dataset";
    exit 1;
fi

ls $CWD/cfgs/"$dataset" | grep yaml | while read line; do
    echo "============================== $line =============================="
    stem=$(echo ${line::-5})

    python $CWD/../train.py \
        --dataset $dataset --dataset_path $CWD/data/$dataset/train \
        --validation_set_path $CWD/data/$dataset/test \
        --config_path $CWD/cfgs/$dataset/$line --experiments_dir $CWD/experiments/$dataset

    find $CWD/experiments/$dataset/$stem | grep "nan" && \
        ( echo "Found nans in ckpts $(find $CWD/experiments/$dataset/$stem)"; kill $$ )
    weights_file=$(find $CWD/experiments/$dataset/$stem -name "*epoch*" | sort | tail -n 1)
    python $CWD/../inference.py $CWD/data/$dataset/test $weights_file -o_npz $CWD/results_npz -o_png $CWD/results_png
done

rm -rf $CWD/results/
