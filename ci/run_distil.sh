#!/usr/bin/bash
set -ex
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

# Train teacher
export EXPERIMENTS_DIR=$CWD/experiments/dronescapes
$CWD/../train.py --dataset dronescapes --dataset_path $CWD/data/dronescapes/train \
    --validation_set_path $CWD/data/dronescapes/test --config_path $CWD/cfgs/dronescapes/safeuav_mmae.yaml \
    --experiments_dir $EXPERIMENTS_DIR

# Export distil dataset
export SUBDIR=$(ls "$EXPERIMENTS_DIR"/safeuav_mmae | tail -n 1)
export SUBDIR2=$(ls "$EXPERIMENTS_DIR"/safeuav_mmae/$SUBDIR | tail -n 1)
export EXPERIMENT_PATH="$EXPERIMENTS_DIR"/safeuav_mmae/$SUBDIR/$SUBDIR2
$CWD/../inference.py $CWD/data/dronescapes/train/ $EXPERIMENT_PATH/checkpoints/last.ckpt -o_npz $EXPERIMENT_PATH/results \
    --exported_tasks semantic_mask2former_swin_mapillary_converted depth_dpt --n_ensembles 2

ln -s $CWD/data/dronescapes/train/rgb/ $EXPERIMENT_PATH/results
ln -s $CWD/data/dronescapes/train/hsv/ $EXPERIMENT_PATH/results

# Train student
$CWD/../train.py --dataset dronescapes --dataset_path $CWD/data/dronescapes/train \
    --validation_set_path $CWD/data/dronescapes/test \
    --config_path $CWD/cfgs/dronescapes/distil/safeuav_mmae_distil.yaml --experiments_dir $EXPERIMENTS_DIR

# Export predictions with both models
export SUBDIR=$(ls "$EXPERIMENTS_DIR"/safeuav_mmae_distil | tail -n 1)
export SUBDIR2=$(ls "$EXPERIMENTS_DIR"/safeuav_mmae_distil/$SUBDIR | tail -n 1)
export EXPERIMENT_PATH_DISTIL="$EXPERIMENTS_DIR"/safeuav_mmae_distil/$SUBDIR/$SUBDIR2

$CWD/../inference.py $CWD/data/dronescapes/train/ $EXPERIMENT_PATH/checkpoints/last.ckpt \
    -o_npz $EXPERIMENT_PATH/results_test --exported_tasks semantic_mask2former_swin_mapillary_converted --n_ensembles 2
$CWD/../inference.py $CWD/data/dronescapes/train/ $EXPERIMENT_PATH_DISTIL/checkpoints/last.ckpt \
    -o_npz $EXPERIMENT_PATH_DISTIL/results_test --exported_tasks semantic_mask2former_swin_mapillary_converted \
    --n_ensembles 2

# Note: we use semantic_segprop8 below because no GT for semantic_mask2former_swin_mapillary_converted as it is
# task mapped. Also, we remove the extra files (another_one.npz) used for other purposes, also w/o GT.
rm $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/another_one.npz
rm $EXPERIMENT_PATH_DISTIL/results_test/semantic_mask2former_swin_mapillary_converted/another_one.npz
$CWD/../scripts/evaluate_semantic_segmentation.py \
    $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted \
    $CWD/data/dronescapes/test/semantic_segprop8 \
    -o $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/res.csv \
    --classes land forest residential road little-objects water sky hill

$CWD/../scripts/evaluate_semantic_segmentation.py \
    $EXPERIMENT_PATH_DISTIL/results_test/semantic_mask2former_swin_mapillary_converted \
    $CWD/data/dronescapes/test/semantic_segprop8 \
    -o $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/res.csv \
    --classes land forest residential road little-objects water sky hill
