#!/usr/bin/bash
set -ex
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

for dataset in $(ls $CWD/data); do
    echo "Chosen dataset: $dataset"
    bash $CWD/run_one.sh $dataset
done
