#!/usr/bin/env python3
"""trainer for neo transformers project"""
from __future__ import annotations
import os
os.environ["LME_LOGLEVEL"] = "0" if int(X := os.getenv("LME_LOGLEVEL", "0")) == 1 else X
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Callable
from datetime import datetime
from functools import partial
from omegaconf import DictConfig, OmegaConf
from loggez import loggez_logger as logger
from lightning_module_enhanced.callbacks import PlotMetrics, PlotCallbackGeneric
from lightning_module_enhanced import schedulers, LME
import torch as tr
from pytorch_lightning import Trainer
from pytorch_lightning.utilities import rank_zero_only
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from torch.utils.data import DataLoader
from torch import optim
from vre.readers import MultiTaskDataset

from algorithms import build_algorithm
from utils import build_metrics, build_loss_computer
from plots import vre_plot_fn, mae_plot_fn, ff_plot_fn
from models import build_model
from readers import VITMultiTaskDataset, build_representations

def _build_plot_fn(cfg: DictConfig, train_reader: VITMultiTaskDataset) -> dict[str, Callable]:
    _plot_fn = {"mae_task_mask": mae_plot_fn, "feed_forward": ff_plot_fn}[cfg.train.algorithm.type]
    return partial(_plot_fn, plot_fns=dict(zip(train_reader.task_names,
                                               [partial(vre_plot_fn, node=n) for n in train_reader.tasks])))

def build_readers(dataset: str, dataset_path: str, validation_set_path: str,
                  cfg: DictConfig) -> tuple[VITMultiTaskDataset, VITMultiTaskDataset]:
    """
    Builds the training and validation sets (both required). Also, adds 'replicas' to the validation set in case we are
    doing DDP training, as the pytorch lightning's default implementation shards both sets, so we make N copies of it.
    """
    representations = build_representations(dataset)
    representations = {k: v for k, v in representations.items() if k in cfg.data.parameters.task_names}
    assert (diff := set(cfg.data.parameters.task_names).difference(representations)) == set(), (diff, representations)
    task_types = {k: v for k, v in representations.items() if k in cfg.data.parameters.task_names}

    base_train_reader = MultiTaskDataset(dataset_path, task_types=task_types, **cfg.data.parameters)
    base_val_reader = MultiTaskDataset(validation_set_path, task_types=task_types,
                                       statistics=base_train_reader.statistics, **cfg.data.parameters)
    train_reader = VITMultiTaskDataset(base_train_reader, augmentation=cfg.train.get("augmentation"))
    n_replicas = 1 if not tr.cuda.is_available() else max(tr.cuda.device_count(), 1)
    val_reader = VITMultiTaskDataset(base_val_reader, n_replicas=n_replicas, augmentation=None)
    return train_reader, val_reader

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True)
    parser.add_argument("--dataset", required=True, choices=["dronescapes", "neo"], default="neo")
    parser.add_argument("--dataset_path", type=Path, required=True)
    parser.add_argument("--validation_set_path", type=Path, required=True)
    parser.add_argument("--experiments_dir", default="experiments/")
    parser.add_argument("--resume_from", type=lambda p: Path(p).absolute())
    parser.add_argument("--end_fast", action="store_true")
    args = parser.parse_args()
    return args

def main(args: Namespace):
    """main fn"""
    cfg = OmegaConf.load(args.config_path)
    train_reader, val_reader = build_readers(args.dataset, args.dataset_path, args.validation_set_path, cfg=cfg)
    cfg.model.hparams.data_shape = {k: train_reader.data_shape[k] for k in train_reader.task_names}
    model = LME(build_model(cfg))
    model.hparams.cfg = OmegaConf.to_container(cfg, resolve=True)
    model.metrics = build_metrics(cfg.train.metrics, train_reader.name_to_task)
    loss_computer = build_loss_computer(cfg.train.criterion, list(model.metrics))
    model.model_algorithm = build_algorithm(cfg.model.type, cfg.train.algorithm, loss_computer=loss_computer)

    if train_reader.statistics is not None:
        model.hparams.statistics = {k: [_v.tolist() for _v in v] for k, v in train_reader.statistics.items()}
    model.optimizer = getattr(optim, cfg.train.optimizer.type)(model.parameters(), **cfg.train.optimizer.parameters)
    if hasattr(cfg.train, "scheduler") and cfg.train.scheduler is not None:
        scheduler_type = getattr(schedulers, cfg.train.scheduler.type)
        model.scheduler = {"scheduler": scheduler_type(model.optimizer, **cfg.train.scheduler.parameters),
                           "monitor": cfg.train.scheduler.monitor}
    model.callbacks = [PlotCallbackGeneric(_build_plot_fn(cfg, train_reader), mode="random"),
                       ModelCheckpoint(every_n_epochs=5, save_top_k=-1), PlotMetrics()]
    model.checkpoint_monitors = cfg.train.checkpoint_monitors
    print(model.summary)
    if args.end_fast:
        exit()

    train_loader = DataLoader(train_reader, **{**cfg.data.loader_params, "shuffle": True},
                              collate_fn=val_reader.collate_fn)
    val_loader = DataLoader(val_reader, **cfg.data.loader_params, collate_fn=val_reader.collate_fn)

    pl_loggers = [CSVLogger(save_dir=f"{args.experiments_dir}/{args.config_path.stem}",
                            name=datetime.strftime(datetime.now(), "%Y%m%d"),
                            version=datetime.strftime(datetime.now(), "%H%M%S"))]
    Path(pl_loggers[0].log_dir).mkdir(parents=True, exist_ok=True) if rank_zero_only.rank == 0 else None
    OmegaConf.save(cfg, f"{pl_loggers[0].log_dir}/config.yaml", resolve=True) if rank_zero_only.rank == 0 else None
    logger.info(f"Log dir: '{pl_loggers[0].log_dir}'")
    trainer = Trainer(logger=pl_loggers, **cfg.train.trainer_params)
    trainer.fit(model, train_loader, val_loader, None, args.resume_from)

if __name__ == "__main__":
    main(get_args())
