"""init file"""
from __future__ import annotations
import sys
from pathlib import Path
from typing import Callable, Any
from functools import partial
from omegaconf import OmegaConf, DictConfig
from loggez import loggez_logger as logger
from lightning_module_enhanced import LME, ModelAlgorithmOutput

sys.path.append(Path(__file__).parents[1].__str__())
from utils import LossComputer, build_masking_algorithm

from .mae_algorithms import conv_mae_algorithm, vit_mae_algorithm
from .ff_algorithms import conv_ff_algorithm # TODO

def build_algorithm(model_type: str, algo_cfg: DictConfig, loss_computer: LossComputer | None) \
        -> Callable[[LME, Any], ModelAlgorithmOutput]:
    """
    Build algorithms for NEO Transformer
    cfg The training configuration file used for this run
    name_to_task A dictionary that maps the name of the task to the dataset task object. Ideally this object would have
    n_dims as well, but it doesn't.
    """
    res = None
    if algo_cfg.type == "mae_task_mask":
        masking_algorithm = build_masking_algorithm(algo_cfg.masking.type, algo_cfg.masking.parameters)
        logger.info(f"Masking Algorithm:\n{masking_algorithm}")
        if model_type == "safeuav":
            res = partial(conv_mae_algorithm, masking_algorithm=masking_algorithm, loss_computer=loss_computer,
                          metrics_only_on_masked=algo_cfg.metrics_only_on_masked)
        if model_type in ("vit_mmae0", "convit_mae0"):
            res = partial(vit_mae_algorithm, masking_algorithm=masking_algorithm, loss_computer=loss_computer,
                          patch_size=tuple(algo_cfg.patch_size), metrics_only_on_masked=algo_cfg.metrics_only_on_masked)

    assert res is not None, algo_cfg.type
    logger.info(f"Built Algorithm: '{algo_cfg.type}'. Params: {OmegaConf.to_container(algo_cfg, resolve=True)}")
    return res
