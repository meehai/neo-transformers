"""simple feed forward algorithm"""
from __future__ import annotations
import sys
from pathlib import Path
from lightning_module_enhanced import LME, ModelAlgorithmOutput

sys.path.append(Path(__file__).parents[1].__str__())
from utils import LossComputer

def conv_ff_algorithm(model: LME, batch: dict, loss_computer: LossComputer | None) -> ModelAlgorithmOutput:
    """SafeUAV Feed forward Algorithm implementation"""
    raise NotImplementedError
