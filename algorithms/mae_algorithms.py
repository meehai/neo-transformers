from pathlib import Path
from typing import NamedTuple
import sys
sys.path.append(Path(__file__).absolute().parents[1].__str__())
from lightning_module_enhanced import LME
import torch as tr

from utils import (MaskingAlgorithm, LossComputer, mask_tensor, compute_metrics,
                   split_pred_tensor, patch_tensor, is_sorted, fill_masked_with_value)

YPredMask = NamedTuple("y", pred=tr.Tensor, mask=tr.Tensor)
MAEAlgorithmOutput = NamedTuple("MAEAlgorithmOutput", y=YPredMask, metrics=dict[str, tr.Tensor],
                                x=dict, gt=dict[str, tr.Tensor])

def conv_mae_algorithm(model: LME, x: dict, masking_algorithm: MaskingAlgorithm,
                       loss_computer: LossComputer | None, metrics_only_on_masked: bool) -> MAEAlgorithmOutput:
    drop_percent = masking_algorithm() # masking_algorithm can also 'prune' some tasks. We add them back.
    drop_percent = {**drop_percent, **{k: 100 for k in x["data"].keys() if k not in drop_percent.keys()}}
    h, w = next(iter(x["data"].values()))[0].shape[1:3]
    gt = {k: patch_tensor(tr.stack(x["data"][k]), (h, w)) for k in x["data"].keys()} # 1 patch only
    x_masked_mask = {k: mask_tensor(v, drop_percent[k]) for k, v in gt.items()}
    x_masked = {task: x_masked_mask[task][0] for task in x_masked_mask}
    masks = {task: x_masked_mask[task][1] for task in x_masked_mask} # store original masks for return/metrics

    x_masked_filled = {k: fill_masked_with_value(v, masks[k], value=0) for k, v in x_masked.items()}

    assert is_sorted(x_masked_filled.keys()), x_masked.keys()
    x_cat = tr.cat(list(x_masked_filled.values()), dim=1)[..., 0].nan_to_num(0, 0, 0) # (B, sum(ch), H, W)
    y_cat = model(x_cat)
    y = split_pred_tensor(y_cat, task_ch=x["image_shape"][0])
    metrics = compute_metrics(y=y, gt=gt, masks=masks if metrics_only_on_masked else None, metrics=model.metrics)
    metrics = metrics if loss_computer is None else {**metrics, "loss": loss_computer(metrics)}
    if loss_computer is not None and metrics["loss"] is None:
        return
    return MAEAlgorithmOutput(y=(YPredMask(y, masks)), metrics=metrics, x=x_cat, gt=gt)

def vit_mae_algorithm(model: LME, x: dict, masking_algorithm: MaskingAlgorithm, patch_size: tuple[int, int],
                      loss_computer: LossComputer | None, metrics_only_on_masked: bool) -> MAEAlgorithmOutput:
    drop_percent = masking_algorithm() # masking_algorithm can also 'prune' some tasks. We add them back.
    drop_percent = {**drop_percent, **{k: 100 for k in x["data"].keys() if k not in drop_percent.keys()}}
    assert is_sorted(x["data"].keys()), x["data"].keys()

    x_stack = {k: tr.stack(x["data"][k]).nan_to_num(0, 0, 0) for k in x["data"]}
    y, y_masks = model(x_stack, drop_percent)
    gt = {k: patch_tensor(v, patch_size) for k, v in x_stack.items()}

    metrics = compute_metrics(y=y, gt=gt, masks=y_masks if metrics_only_on_masked else None, metrics=model.metrics)
    metrics = metrics if loss_computer is None else {**metrics, "loss": loss_computer(metrics)}
    if loss_computer is not None and metrics["loss"] is None:
        return

    return MAEAlgorithmOutput(y=YPredMask(y, y_masks), metrics=metrics, x=x_stack, gt=gt)
