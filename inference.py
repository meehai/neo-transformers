#!/usr/bin/env python3
"""inference for neo transformers project"""
from __future__ import annotations
import os
os.environ["MPL_LOGLEVEL"] = "0"
os.environ["VRE_LOGLEVEL"] = "0"
os.environ["LOGGEZ_LOGLEVEL"] = "0"
from argparse import ArgumentParser, Namespace
import shutil
from pathlib import Path
from typing import Callable
from tqdm import tqdm
from omegaconf import DictConfig
from functools import partial
from loggez import loggez_logger as logger
import torch as tr
import numpy as np
from torch.utils.data import DataLoader
from lightning_module_enhanced import LME, ModelAlgorithmOutput
from lightning_module_enhanced.utils import to_device
from vre.readers import MultiTaskDataset
from vre.utils import collage_fn, image_write
from pytorch_lightning import Trainer

from readers import VITMultiTaskDataset, build_representations
from models import build_model
from algorithms import build_algorithm
from plots import vre_plot_fn
from utils import oc_argparse_overwrite, mean

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def store_png(out_dir: Path, ys: dict[str, tr.Tensor], gts: dict[str, tr.Tensor], names: list[str],
              plot_fns: dict[str, Callable]):
    """stores the images of this batch so we don't store them in memory until the end"""
    assert all(k in gts.keys() and k in plot_fns.keys() for k in ys.keys()), (ys.keys(), gts.keys(), plot_fns.keys())
    out_dir.mkdir(exist_ok=True, parents=True)
    for i in range(len(names)):
        plots = []
        for task in list(ys):
            if ys[task][i] is None:
                continue
            y_i_t, gt_i_t = ys[task][i].permute(1, 2, 0), gts[task][i].permute(1, 2, 0)
            gt_img = plot_fns[task](gt_i_t)
            y_img = plot_fns[task](y_i_t)
            if not (M := gt_img.sum(-1, keepdims=True) == 0).all(): # if whole gt is missing, show prediction as is
                y_img = y_img * ~M
            plots.append(collage_fn([y_img, gt_img]))
        image_write(collage_fn(plots) if len(plots) > 1 else plots[0], out_dir / f"{names[i][0:-4]}.png")

def store_npz(out_dir: Path, ys: dict[str, tr.Tensor], names: list[str]):
    """Stores the predictions of a btch so they can be evaluated offline as well"""
    out_dir.mkdir(exist_ok=True, parents=True)
    for i in range(len(names)):
        for task in list(ys):
            if ys[task][i] is None:
                continue
            (out_dir / task).mkdir(exist_ok=True)
            y = ys[task][i].permute(1, 2, 0).numpy()
            y = y.astype(np.float16) if os.getenv("FLOAT16", "0") == "1" and y.dtype == np.float32 else y
            np.savez_compressed(out_dir / task / f"{names[i][0:-4]}.npz", y)

def store_ensemble_npz(out_dir: Path, ys_ensemble: dict[str, list[tr.Tensor]], names: list[str]):
    """Stores the predictions of a btch so they can be evaluated offline as well"""
    for task in ys_ensemble.keys():
        # create a directory for each task/name.npz that'll hold all the underlying 0.npz, ..., n.npz ens predictions
        for name in names:
            shutil.rmtree(Path(out_dir / task / name), ignore_errors=True)
            Path(out_dir / task / name).mkdir(parents=True)
        for ens_ix in range(len(ys_ensemble[task])):
            if ys_ensemble[task][ens_ix] is None:
                continue
            ens_np = ys_ensemble[task][ens_ix].permute(0, 2, 3, 1).numpy() # (B, H, W, C)
            for name_ix, name in enumerate(names):
                assert not (out_path := out_dir / task / name / f"{ens_ix}.npz").exists(), out_path
                np.savez_compressed(out_path, ens_np[name_ix])

def predict_algorithm(model, batch, cfg: DictConfig, model_algorithm: Callable, plot_fns: dict[str, Callable],
                      tasks: list[str], exported_tasks: list[str], n_ensembles: int, output_path_png: Path | None,
                      output_path_npz: Path | None, output_path_ensemble_npz: Path | None) -> ModelAlgorithmOutput:
    if cfg.train.algorithm.masking.get("type", "") == "fixed_ordered_masking":
            model_algorithm.keywords["masking_algorithm"].reset()
    if all([(output_path_npz / "semantic_output" / name).exists() for name in batch["name"]]):
        return [None]

    ys, gts, ys_ensemble = {task: [] for task in tasks}, {task: [] for task in tasks}, {task: [] for task in tasks}
    for _ in range(n_ensembles):
        y_model: ModelAlgorithmOutput = to_device(model_algorithm(model, batch).y.pred, "cpu")
        for task in tasks:
            ys_ensemble[task].append(y_model[task]) # some algos may not output all tasks

    for task in tasks:
        ys[task].extend(mean(ys_ensemble[task]) if len(ys_ensemble[task]) > 0 else [None] * len(batch["name"]))
        gts[task].extend(batch["data"][task])

    ys = {k: v for k, v in ys.items() if k in exported_tasks}
    if output_path_png is not None:
        store_png(output_path_png, ys, gts, batch["name"], plot_fns)
    if output_path_npz is not None:
        store_npz(output_path_npz, ys, batch["name"])
    if output_path_ensemble_npz is not None:
        store_ensemble_npz(output_path_ensemble_npz, ys_ensemble, batch["name"])
    return [None]

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("test_set_path", type=Path)
    parser.add_argument("weights_path", type=Path)
    parser.add_argument("--end_fast", action="store_true")
    parser.add_argument("--n_ensembles", type=int, default=1)
    parser.add_argument("--output_path_png", "-o_png", type=Path)
    parser.add_argument("--output_path_npz", "-o_npz", type=Path)
    parser.add_argument("--output_path_ensemble_npz", "-o_ens_npz", type=Path)
    parser.add_argument("--replace_cfg", action="append", nargs="*", default=[])
    parser.add_argument("--exported_tasks", nargs="+")
    args = parser.parse_args()
    assert args.output_path_npz is not None or args.output_path_png is not None \
        or args.output_path_ensemble_npz is not None, "at least one must be set"
    assert args.n_ensembles > 1 or args.output_path_ensemble_npz is None, args.n_ensembles
    return args

@tr.no_grad()
def main(args: Namespace):
    """main fn"""
    data = tr.load(args.weights_path, map_location="cpu")
    assert (cfg := DictConfig(data["hyper_parameters"]["cfg"])).data.parameters.handle_missing_data == "fill_nan"
    oc_argparse_overwrite(cfg, args)
    task_types = build_representations(cfg.data.dataset)
    test_base_reader = MultiTaskDataset(args.test_set_path, task_types=task_types, **cfg.data.parameters,
                                        statistics=data["hyper_parameters"]["statistics"])
    test_reader = VITMultiTaskDataset(test_base_reader)
    test_loader = DataLoader(test_reader, **cfg.data.loader_params, collate_fn=test_reader.collate_fn)
    plot_fns = dict(zip(test_reader.task_names, [partial(vre_plot_fn, node=n) for n in test_reader.tasks]))
    model = LME(build_model(cfg).to(device).eval())
    model.load_state_dict(data["state_dict"])
    model_algorithm = build_algorithm(cfg.model.type, cfg.train.algorithm, loss_computer=None)

    tasks = args.exported_tasks = test_reader.task_names if args.exported_tasks is None else args.exported_tasks

    logger.info(f"Loaded '{cfg.model.type}' with {model.num_params} parameters from '{args.weights_path}'")
    logger.info(f"Tasks: {tasks}. Tasks to be stored: {args.exported_tasks}. N ensemble: {args.n_ensembles}")
    logger.info(f"Excluded (fully masked) tasks: {cfg.train.algorithm.masking.parameters.excluded_tasks}")
    if args.end_fast:
        return

    model.model_algorithm = partial(predict_algorithm, cfg=cfg, model_algorithm=model_algorithm, plot_fns=plot_fns,
                                    tasks=tasks, exported_tasks=args.exported_tasks, n_ensembles=args.n_ensembles,
                                    output_path_png=args.output_path_png, output_path_npz=args.output_path_npz,
                                    output_path_ensemble_npz=args.output_path_ensemble_npz)
    Trainer(**cfg.train.trainer_params).predict(model, test_loader)

    exported_dict = {"npz": args.output_path_npz, "png": args.output_path_png,
                     "npz_ensembles": args.output_path_ensemble_npz}
    exported_str = ", ".join([f"'{v}' ({k})" for k, v in exported_dict.items() if v is not None])
    logger.info(f"Exported {args.exported_tasks} to {exported_str}")

if __name__ == "__main__":
    main(get_args())
