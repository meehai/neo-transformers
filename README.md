# NEO and Dronescapes-experts playground

Script to train, evaluate, make ensembles and pseudo-labels for these two multi task datasets.

### Training and evaluation

Training a model is as simple as:
```bash
export EXPERIMENTS_DIR=/home/mihai/code/ml/neo-transformers/ci/experiments/dronescapes
./train.py --dataset dronescapes --dataset_path /home/mihai/code/ml/neo-transformers/ci/data/dronescapes/train --validation_set_path /home/mihai/code/ml/neo-transformers/ci/data/dronescapes/test --config_path /home/mihai/code/ml/neo-transformers/ci/cfgs/dronescapes/safeuav_mmae.yaml --experiments_dir $EXPERIMENTS_DIR
```

Evaluation is done by first exporting the results and then running the evalation script:
```bash
./inference.py ci/data/dronescapes/test/ $EXPERIMENT_PATH/checkpoints/last.ckpt -o_npz $EXPERIMENT_PATH/results_test --exported_tasks semantic_mask2former_swin_mapillary_converted --n_ensembles 2

./scripts/evaluate_semantic_segmentation.py $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted ci/data/dronescapes/test/semantic_segprop8 -o $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/res.csv --classes land forest residential road little-objects water sky hill
```

See `scripts/evaluate_regression.py` as well.

### Full Evaluation of a directory of checkpoints

Given a directory structure of
```bash
ls ckpts/safeuav/sema/*/*.ckpt
'ckpts/safeuav/sema/mae-10M-ext/epoch=23-val_semantic_output_mean_iou=0.448.ckpt'
'ckpts/safeuav/sema/mtl-1M-ext-distil/epoch=10-val_semantic_output_mean_iou=0.450.ckpt'
'ckpts/safeuav/sema/mtl-4M-ext-distil/epoch=16-val_semantic_output_mean_iou=0.450.ckpt'
'ckpts/safeuav/sema/mtl-4M-ext-distil/epoch=25-val_semantic_output_mean_iou=0.451.ckpt'
'ckpts/safeuav/sema/mae-4M-ext/epoch=37-val_semantic_output_mean_iou=0.470.ckpt'
'ckpts/safeuav/sema/mae-4M-train/epoch=1-val_loss=0.456.ckpt'
```

You can run the [ensemble and eval](scripts/ensemble_and_eval.sh) script on it. As it looks for .ckpt files first using
`find`, it should work even if the ckpts are a few levels deep like here.
```bash
bash script/ensemle_and_eval.sh ckpts/safeuav/sema/ # this will run for all the ckpts above
bash script/ensemle_and_eval.sh ckpts/safeuav/sema/mtl-4M-ext-distil # this will run for those 2 ckpts only
```

It should produce outputs for a single ensemble for non-mae variants and outputs (1 dir of predictions each) for
`1 2 3 5 10 20 30 50` ensembles for mae variants.

### Distillation

First, train your model:
```bash
export EXPERIMENTS_DIR=/home/mihai/code/ml/neo-transformers/ci/experiments/dronescapes
./train.py --dataset dronescapes --dataset_path /home/mihai/code/ml/neo-transformers/ci/data/dronescapes/train --validation_set_path /home/mihai/code/ml/neo-transformers/ci/data/dronescapes/test --config_path /home/mihai/code/ml/neo-transformers/ci/cfgs/dronescapes/safeuav_mmae.yaml --experiments_dir $EXPERIMENTS_DIR
```

Then, export pseudo-labels on your desired dataset. Grab the exported path from above (has timestamps).
In this case, also the train set, but we do some ensembling.
```bash
export EXPERIMENT_PATH="$EXPERIMENTS_DIR/safeuav_mmae/20250128/092038/"
./inference.py ci/data/dronescapes/train/ $EXPERIMENT_PATH/checkpoints/last.ckpt -o_npz $EXPERIMENT_PATH/results --exported_tasks semantic_mask2former_swin_mapillary_converted depth_dpt --n_ensembles 2

ln -s `pwd`/ci/data/dronescapes/train/rgb/ $EXPERIMENT_PATH/results
ln -s `pwd`/ci/data/dronescapes/train/hsv/ $EXPERIMENT_PATH/results
```

Third, we train the new model using the same checkpoint but the new data!
```bash
./train.py --dataset dronescapes --dataset_path $EXPERIMENT_PATH/results --validation_set_path /home/mihai/code/ml/neo-transformers/ci/data/dronescapes/test --config_path /home/mihai/code/ml/neo-transformers/ci/cfgs/dronescapes/distil/safeuav_mmae.yaml --experiments_dir $EXPERIMENTS_DIR
export EXPERIMENT_PATH_DISTIL="$EXPERIMENTS_DIR/safeuav_mmae_distil/20250128/113145/"
```

Finally, evaluation. We start by exporting predictions on the test set with both checkpoints.
```bash
./inference.py ci/data/dronescapes/test/ $EXPERIMENT_PATH/checkpoints/last.ckpt -o_npz $EXPERIMENT_PATH/results_test --exported_tasks semantic_mask2former_swin_mapillary_converted --n_ensembles 2
./inference.py ci/data/dronescapes/test/ $EXPERIMENT_PATH_DISTIL/checkpoints/last.ckpt -o_npz $EXPERIMENT_PATH_DISTIL/results_test --exported_tasks semantic_mask2former_swin_mapillary_converted --n_ensembles 2
```

And then run the evaluation script.
```bash
./scripts/evaluate_semantic_segmentation.py $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted ci/data/dronescapes/test/semantic_segprop8 -o $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/res.csv --classes land forest residential road little-objects water sky hill --allow_subset

./scripts/evaluate_semantic_segmentation.py $EXPERIMENT_PATH_DISTIL/results_test/semantic_mask2former_swin_mapillary_converted ci/data/dronescapes/test/semantic_segprop8 -o $EXPERIMENT_PATH/results_test/semantic_mask2former_swin_mapillary_converted/res.csv --classes land forest residential road little-objects water sky hill --allow_subset
```

Notes:
- `semantic_segprop8` is used because `semantic_mask2former_swin_mapillary_converted` is actually task mapped
- `--allow_subset` is used because there is a file `another_one.npz` in the CI data with no GT data used to test the missing data code. We could delete it explicitly and remove the flag in the evaluation script
