"""init file"""
from vre import Representation
from .vit_multitask_dataset import VITMultiTaskDataset
from .neo_representations import neo_task_types
from .dronescapes_representations import get_dronescapes_task_types

def build_representations(dataset: str) -> dict[str, Representation]:
    """all the datasets supported in this project"""
    if dataset == "neo":
        representations = neo_task_types
    elif dataset == "dronescapes":
        representations = get_dronescapes_task_types(include_semantics_original=False, include_gt=True, include_ci=True)
    else:
        raise ValueError(f"unknown dataset: '{dataset}'")
    return representations
