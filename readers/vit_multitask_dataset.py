#!/usr/bin/env python3
#%%
from __future__ import annotations
import sys
import os
from pathlib import Path
from types import SimpleNamespace
from functools import partial
import pytest
import random
from lovely_tensors import monkey_patch
import numpy as np
import torch as tr
from torch.utils.data import Dataset, DataLoader
from torch.nn import functional as F
import matplotlib.pyplot as plt
from loggez import loggez_logger as logger
from vre.utils import collage_fn, image_resize, image_write, image_add_title
from vre.readers.multitask_dataset import MultiTaskDataset, MultiTaskItem
sys.path.extend([Path(__file__).parents[0].__str__(), Path(__file__).parents[1].__str__(),
                 Path.cwd().parents[0].__str__(), Path(__file__).cwd().__str__()])

from utils import patch_tensor, unpatch_with_size, mask_tensor
from plots import mae_plot_fn, vre_plot_fn
from algorithms.mae_algorithms import MAEAlgorithmOutput, YPredMask
from neo_representations import neo_task_types
from dronescapes_representations import get_dronescapes_task_types

monkey_patch()

# %load_ext autoreload
# %autoreload 2

def crop_augment(chw_data: dict[str, tr.Tensor], activation_percent: float, min_area_percent: float,
                 max_area_percent: float, classification_tasks: list[str] | None = None) -> dict[str, tr.Tensor]:
    """Crops the dict of tensors with boxes in [min_area_pct: max_area_pct] based on H*W area"""
    if random.random() * 100 > activation_percent:
        return chw_data

    # Get the shape of the image (assuming all images are the same size)
    classification_tasks = classification_tasks or []
    _, H, W = chw_data[next(iter(chw_data))].shape

    # Calculate the minimum area that should remain after cropping
    original_area = H * W
    min_area = original_area * min_area_percent / 100
    max_area = original_area * max_area_percent / 100

    n_tries = 0
    while True:
        n_tries += 1
        # Randomly pick top-left corner (x1, y1)
        x1 = random.randint(0, W-1)
        y1 = random.randint(0, H-1)
        # Randomly pick bottom-right corner (x2, y2)
        x2 = random.randint(x1, W)
        y2 = random.randint(y1, H)
        # Check if the crop area is larger than the required minimum area
        crop_area = (x2 - x1) * (y2 - y1)
        if min_area <= crop_area <= max_area:
            break

    res = {}
    # Apply the same crop and resize to all items in the dictionary
    for task, data in chw_data.items():
        cropped = data[:, y1:y2, x1:x2]
        mode = "bilinear" if task not in classification_tasks else "nearest"
        align_corners = False if task not in classification_tasks else None
        resized = F.interpolate(cropped.unsqueeze(0), size=(H, W), mode=mode, align_corners=align_corners).squeeze(0)
        res[task] = resized
    return res


#%%
class VITMultiTaskDataset(Dataset):
    """Extends MultiTaskDataset to work with transformers by patching each task."""
    def __init__(self, base_dataset: MultiTaskDataset, n_replicas: int = 1,
                 augmentation: dict[str, str | dict[str, str]] | None = None):
        assert base_dataset.handle_missing_data in ("fill_nan", "drop"), base_dataset.handle_missing_data
        if base_dataset.normalization is None or not all(x in ("min_max", "standardization")
                                                         for x in base_dataset.normalization.values()):
            logger.warning(f"Not all normalizations are 'min_max', or 'standardization'. Hope you know what you do")
        self.base_dataset = base_dataset
        self.n_replicas = n_replicas
        self.augmentation = augmentation

        self.h, self.w = list((dshapes := base_dataset.data_shape).values())[0][0:2]
        # these checks below are because MultiTaskDataset is more generic than just equally (h, w) tasks
        assert all(x[0:2] == (self.h, self.w) for x in base_dataset.data_shape.values()), (dshapes, self.h, self.w)
        assert all(len(v) in (2, 3) for v in base_dataset.data_shape.values()), base_dataset.data_shape
        self.ch = {k: v[-1] if len(v) == 3 else 1 for k, v in base_dataset.data_shape.items()}
        self.data_shape = {k: (self.ch[k], self.h, self.w) for k in self.ch.keys()} # (C, H, W)


    def collate_fn(self, batch: list[MultiTaskItem]) -> MultiTaskItem:
        """collate fn for data loader"""
        data = {k: [b["data"][k] for b in batch] for k in self.task_names} # TODO: perf issues ?
        res = {"data": data, "image_shape": (self.ch, self.h, self.w),
               "name": [item["name"] for item in batch], "task_types": self.tasks}
        return res

    def __getattr__(self, key: str):
        if key in ("tasks", "task_names", "name_to_task", "statistics"):
            return getattr(self.base_dataset, key)
        return super().__getattribute__(key)

    def __getitem__(self, ix: int | slice | list[int]) -> MultiTaskItem:
        if isinstance(ix, list):
            return [self[_ix] for _ix in ix]
        if isinstance(ix, slice):
            return [self[_ix] for _ix in range(ix.start, ix.stop)]
        if isinstance(ix, str):
            return self[self.base_dataset.file_names.index(ix)]

        classif_tasks = [t.name for t in self.base_dataset.classification_tasks]
        raw_data, name = self.base_dataset[ix % len(self.base_dataset)]
        _val = float("nan") if self.base_dataset.handle_missing_data == "fill_nan" else 0
        chw_data = {k: tr.zeros(self.ch[k], self.h, self.w).fill_(_val) for k in raw_data.keys()} # {task: (C, H, W)}
        for k, v in raw_data.items():
            try:
                chw_data[k][:] = v.unsqueeze(0) if len(v.shape) == 2 else v.permute(2, 0, 1)
            except Exception as e: # production ready
                logger.error(f"Error {e} at {k}/{ix} ({name}): {raw_data[k]}, skipping.")
            if chw_data[k].isinf().any():
                logger.error(f"Inf at {k}/{ix} ({name}): {raw_data[k]}, skipping.")
                chw_data[k] = chw_data[k].nan_to_num(_val, 0, 0)
            if self.base_dataset.normalization is not None and self.base_dataset.normalization[k] is not None:
                if k not in classif_tasks and ((chw_data[k] < -30) | (chw_data[k] > 30)).any():
                    logger.error(f"Big values at {k}/{ix} ({name}): {raw_data[k]}, clipping.")
                    chw_data[k][(chw_data[k] < -30) | (chw_data[k] > 30)] = 0

        if self.augmentation is not None:
            if self.augmentation["type"] == "random_crop":
                classif_tasks = [t.name for t in self.base_dataset.classification_tasks]
                chw_data = crop_augment(chw_data, **self.augmentation["parameters"], classification_tasks=classif_tasks)

        return {"data": chw_data, "name": name}

    def __len__(self) -> int:
        return int(os.getenv("DATASET_LEN", len(self.base_dataset))) * self.n_replicas

    def __repr__(self):
        return f"""
[VITMultiTaskDataset]
- Data shape: {self.data_shape}
- N replicas: {self.n_replicas}
{repr(self.base_dataset)}"""

@pytest.mark.parametrize("dataset,normalization", [
    ("neo", None), ("neo", "min_max"), ("neo", "standardization"),
    ("dronescapes", None), ("dronescapes", "min_max"), ("dronescapes", "standardization")])
def test_VITMultiTaskDataset(dataset: str, normalization: str | None):
    assert dataset in ("neo", "dronescapes"), dataset
    ph, pw = 60, 60
    dataset_path = Path(__file__).parents[1] / f"ci/data/{dataset}/train"
    handle_missing_data = "fill_nan"
    if dataset == "neo":
        task_types = neo_task_types
        task_names = ["AOD", "LSTD_AN"]
    else:
        task_types = get_dronescapes_task_types(include_semantics_original=False, include_gt=True, include_ci=True)
        task_names = ["rgb", "depth_dpt"]
    task_types = {k: task_types[k] for k in task_names}
    base_dataset = MultiTaskDataset(dataset_path, task_names=task_names, handle_missing_data=handle_missing_data,
                                    task_types=task_types, normalization=normalization)
    dataset = VITMultiTaskDataset(base_dataset)
    loader = DataLoader(dataset, batch_size=16, shuffle=True, collate_fn=dataset.collate_fn)
    h, w = dataset.h, dataset.w
    tasks = dataset.task_names
    plot_fns = dict(zip(dataset.task_names, [partial(vre_plot_fn, node=n) for n in dataset.tasks]))

    batch = next(iter(loader))
    stacked_data = {k: tr.stack(batch["data"][k]) for k in batch["data"].keys()}
    patched_data = {k: patch_tensor(stacked_data[k], (ph, pw)) for k in batch["data"].keys()}
    h, w = (image_shape := batch["image_shape"])[1:3]

    y = {k: v for k, v in batch["data"].items()}
    x_masked_masks = {k: mask_tensor(v, 30 if np.random.rand() < 0.7 else 100) for k, v in patched_data.items()}
    x_masked, masks = {k: v[0] for k, v in x_masked_masks.items()}, {k: v[1] for k, v in x_masked_masks.items()}

    masked_batch = {"data": batch["data"], "tasks": tasks, "image_shape": image_shape}
    out_dir = Path(__file__) if __name__ == "__main__" else None
    model = SimpleNamespace(hparams=SimpleNamespace(tasks=tasks, patch_size=(ph, pw)))
    y_algo = MAEAlgorithmOutput(y=YPredMask(y, masks), metrics=None, x=x_masked, gt=patched_data)
    img = mae_plot_fn(model, masked_batch, y_algo, out_dir, plot_fns)
    if __name__ == "__main__":
        plt.imshow(img)

    # put together all tasks in the batch dim, go through unpatch_img and expand them again in (bs, n_tsk)
    unpatched_data = {k: unpatch_with_size(v, stacked_data[k].shape).permute(0, 2, 3, 1)
                      for k, v in patched_data.items()}
    unpatched_task = unpatched_data[(chosen_task := np.random.choice(dataset.task_names))]
    if __name__ == "__main__":
        plt.figure()
        plt.imshow(image_add_title(plot_fns[chosen_task](unpatched_task[0]), chosen_task, size_px=h // 15))

    res = []
    for task in dataset.task_names:
        res.append(item := plot_fns[task](unpatched_data[task][0]))
        assert item.shape == (h, w, 3), (item.shape, (h, w, 3))
    collage = image_resize(collage_fn(res, titles=dataset.task_names), height=2000, width=None)
    ch, cw = collage.shape[0:2]
    collage = image_add_title(collage, batch["name"][0], top_padding=ch // 10, size_px=cw // 50)
    if __name__ == "__main__":
        plt.figure()
        plt.imshow(collage)
        image_write(collage, "collage.png")

def test_crop_augment():
    pth = Path(__file__).parents[1] / f"ci/data/dronescapes/train/rgb/barsana_DJI_0500_0501_combined_sliced_2700_14700_10680.npz"
    a = np.load(pth)["arr_0"]
    b = crop_augment({"rgb": tr.from_numpy(a.astype(np.float32) / 255)}, 100, 30, 80)["rgb"].numpy()
    b = (np.clip(b, 0, 1) * 255).astype(np.uint8)
    assert a.shape == b.shape and a.dtype == b.dtype and a.mean() != b.mean() and a.std() != b.std()

if __name__ == "__main__":
    test_VITMultiTaskDataset("dronescapes", None)


# %%
