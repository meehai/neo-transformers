"""NEO nodes implementation in ngclib repository"""
from __future__ import annotations
from pathlib import Path
import numpy as np
from codecs import encode
from overrides import overrides

from vre.representations import Representation, NpIORepresentation, NormedRepresentationMixin
from vre.utils import ReprOut

def _cmap_hex_to_rgb(hex_list):
    res = []
    for hex_data in hex_list:
        r = int(hex_data[1: 3], 16)
        g = int(hex_data[3: 5], 16)
        b = int(hex_data[5: 7], 16)
        res.append([r, g, b])
    return np.array(res)

def _act_to_cmap(act_file: Path) -> np.ndarray:
    """converts the .act file to a matplotlib cmap representation"""
    with open(act_file, "rb") as act:
        raw_data = act.read()                           # Read binary data
    hex_data = encode(raw_data, "hex")                  # Convert it to hexadecimal values
    total_colors_count = int(hex_data[-7:-4], 16)       # Get last 3 digits to get number of colors total
    total_colors_count = 256

    # Decode colors from hex to string and split it by 6 (because colors are #1c1c1c)
    colors = [hex_data[i: i + 6].decode() for i in range(0, total_colors_count * 6, 6)]

    # Add # to each item and filter empty items if there is a corrupted total_colors_count bit
    hex_colors = [f"#{i}" for i in colors if len(i)]
    rgb_colors = _cmap_hex_to_rgb(hex_colors)
    return rgb_colors

class NEONode(Representation, NpIORepresentation, NormedRepresentationMixin):
    """NEO nodes implementation"""
    def __init__(self, node_type: str, name: str):
        Representation.__init__(self, name=name, dependencies=None)
        NpIORepresentation.__init__(self)
        NormedRepresentationMixin.__init__(self)
        self.node_type = node_type
        self.name = name
        act_path = Path(__file__).absolute().parent / "cmaps" / f"{self.node_type}.act"
        assert act_path.exists(), f"Node type '{node_type}' not valid. No act file found: '{act_path}'"
        self.cmap = _act_to_cmap(act_path)

    @overrides
    def make_images(self, data: ReprOut) -> np.ndarray:
        y = np.clip(data.output, 0, 1)
        y = y[..., 0] if y.shape[-1] == 1 else y
        y = y * 255
        y[np.isnan(y)] = 255
        y = y.astype(np.uint)
        y_rgb = self.cmap[y].astype(np.uint8)
        y_rgb[np.isnan(y) | y == 0] = 0
        return y_rgb

    @property
    @overrides
    def n_channels(self) -> int:
        return 1

# For both 1month and 1week datasets
_name_to_type = {"AOD": "AerosolOpticalDepth", "BS_ALBEDO": "Albedo", "CHLORA": "Chlorophyll",
                 "CLD_FR": "CloudFraction", "CLD_RD": "CloudParticleRadius", "CLD_WP": "CloudWaterContent",
                 "COT": "CloudOpticalThickness", "CO_M": "CarbonMonoxide", "FIRE": "Fire",
                 "INSOL": "SolarInsolation", "LAI": "LeafAreaIndex", "LSTD": "Temperature",
                 "LSTD_AN": "TemperatureAnomaly", "LSTN": "Temperature", "LSTN_AN": "TemperatureAnomaly",
                 "LWFLUX": "OutgoingLongwaveRadiation", "NDVI": "Vegetation", "NETFLUX": "NetRadiation",
                 "NO2": "NitrogenDioxide", "OZONE": "Ozone", "SNOWC": "SnowCover", "SST": "SeaSurfaceTemperature",
                 "SWFLUX": "ReflectedShortwaveRadiation", "WV": "WaterVapor"}
neo_task_types = {k: NEONode(v, k) for k, v in _name_to_type.items()}
