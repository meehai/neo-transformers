import sys
from pathlib import Path
from vre.representations import Representation
from vre_repository.color.rgb import RGB
from vre_repository.color.hsv import HSV
from vre_repository.depth import DepthRepresentation
from vre_repository.normals import NormalsRepresentation
from vre_repository.edges import EdgesRepresentation
from vre_repository.optical_flow import OpticalFlowRepresentation
from vre_repository.semantic_segmentation import SemanticRepresentation

def get_gt_tasks() -> dict[str, Representation]:
    color_map = [[0, 255, 0], [0, 127, 0], [255, 255, 0], [255, 255, 255],
                [255, 0, 0], [0, 0, 255], [0, 255, 255], [127, 127, 63]]
    classes_8 = ["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"]
    tasks = [
        SemanticRepresentation("semantic_output", classes=classes_8, color_map=color_map, disk_data_argmax=True),
        DepthRepresentation("depth_output", min_depth=0, max_depth=300),
        NormalsRepresentation("camera_normals_output"),
    ]
    return {t.name: t for t in tasks}

def get_other_tasks(include_semantics_original: bool, include_ci: bool) -> dict[str, Representation]:
    sys.path.append(str(Path(__file__).parents[1] / "semantic_mapper"))
    from semantic_mapper import (m2f_mapillary, m2f_coco, m2f_r50_mapillary,
                                 BinaryMapper, mapillary_classes, coco_classes)
    tasks = [
        rgb := RGB("rgb"),
        OpticalFlowRepresentation("opticalflow_rife", delta=1),
        DepthRepresentation("depth_marigold", min_depth=0, max_depth=1),
        NormalsRepresentation("normals_svd(depth_marigold)")
    ]

    if include_semantics_original:
        tasks.extend([m2f_mapillary, m2f_coco, m2f_r50_mapillary])
    if include_ci:
        transportation_mapping = [
            {
                "others": [c for c in mapillary_classes if c not in
                        (cls := ["Bike Lane", "Crosswalk - Plain", "Curb Cut", "Parking", "Rail Track", "Road",
                                    "Service Lane", "Sidewalk", "Bridge", "Tunnel", "Bicyclist", "Motorcyclist",
                                    "Other Rider", "Lane Marking - Crosswalk", "Lane Marking - General",
                                    "Traffic Light", "Traffic Sign (Back)", "Traffic Sign (Front)", "Bicycle", "Boat",
                                    "Bus", "Car", "Caravan", "Motorcycle", "On Rails", "Other Vehicle", "Trailer",
                                    "Truck", "Wheeled Slow", "Car Mount", "Ego Vehicle"])],
                "transportation": cls,
            },
            {
                "others": [c for c in coco_classes if c not in
                        (cls := ["bicycle", "car", "motorcycle", "airplane", "bus", "train", "truck", "boat",
                                    "road", "railroad", "pavement-merged"])],
                "transportation": cls,
            },
        ]

        tasks.extend([
            HSV("hsv", [rgb]),
            DepthRepresentation("depth_dpt", min_depth=0, max_depth=1),
            EdgesRepresentation("edges_dexined"),
            BinaryMapper("transportation_ci", [m2f_mapillary, m2f_coco], transportation_mapping, mode="at_least_one"),
        ])

    return {t.name: t for t in tasks}

def get_dronescapes_task_types(include_semantics_original: bool, include_gt: bool,
                               include_ci: bool) -> dict[str, Representation]:
    sys.path.append(str(Path(__file__).parents[1] / "semantic_mapper"))
    from semantic_mapper import get_new_semantic_mapped_tasks
    res = {
        **get_new_semantic_mapped_tasks(),
        **get_other_tasks(include_semantics_original, include_ci),
        **(get_gt_tasks() if include_gt else {}),
    }
    return res

dronescapes_task_types = get_dronescapes_task_types(include_semantics_original=False, include_gt=True, include_ci=False)
