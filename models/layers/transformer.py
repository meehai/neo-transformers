"""Transformer model"""
from __future__ import annotations
import math
import torch as tr
from torch import nn
from torch.nn import functional as F

from lovely_tensors import monkey_patch
monkey_patch()

class AttentionLayer(nn.Module):
    def __init__(self, emb_size: int, head_size: int, dropout: int, causal: bool):
        super().__init__()
        self.dropout = dropout
        self.causal = causal
        self.q = nn.Linear(emb_size, head_size)
        self.k = nn.Linear(emb_size, head_size)
        self.v = nn.Linear(emb_size, head_size)

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None) -> tr.Tensor:
        """x :: (B, t, E), kv :: (B, t2, E)"""
        q = self.q(x) # (B, t, E)
        k = self.k(x if kv is None else kv).permute(0, 2, 1) # (B, E, t2)
        v = self.v(x if kv is None else kv) # (B, t2, E)

        w = q @ k # (B, t, t2)
        w_scaled = w / math.sqrt(q.shape[-1]) # q.shape[-1] == E

        # mask (if causal) & normalize the weights (sum to 1) via softmax
        if self.causal:
            tril = tr.tril(tr.ones_like(w_scaled))
            w_scaled[tril == 0] = -math.inf
        w_scaled = F.softmax(w_scaled, dim=-1)
        w_scaled = F.dropout(w_scaled, self.dropout, training=self.training) # (B, t, t2)

        y_out = w_scaled @ v # (B, t, E)
        return y_out

class MultiHeadAttentionVanilla(nn.Module):
    def __init__(self, emb_size: int, n_heads: int, dropout: int, causal: bool):
        super().__init__()
        assert emb_size % n_heads == 0, (emb_size, n_heads)
        self.dropout = dropout
        self.attention_heads = nn.ModuleList(AttentionLayer(emb_size, emb_size // n_heads, dropout, causal)
                                             for _ in range(n_heads))
        self.fc_out = nn.Linear(emb_size, emb_size)

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None) -> tr.Tensor:
        """x :: (B, t, emb_size), kv :: (B, t, emb_size) | None"""
        assert len(x.shape) == 3 and (kv is None or len(kv.shape) == 3), (x, kv)
        y_heads = [head(x, kv) for head in self.attention_heads] # [(B, t, h_s)]
        y = tr.cat(y_heads, dim=-1) # (B, t, E)
        y_out = self.fc_out(y) # (B, t, E)
        y_out = F.dropout(y_out, self.dropout, training=self.training)
        return y_out

class MultiHeadAttention(nn.Module):
    def __init__(self, emb_size: int, n_heads: int, dropout: int, causal: bool):
        super().__init__()
        assert emb_size % n_heads == 0, (emb_size, n_heads)
        # key, query, value projections for all heads, but in a batch
        self.q, self.k, self.v = (nn.Linear(emb_size, emb_size, bias=False) for _ in range(3))
        self.c_proj = nn.Linear(emb_size, emb_size, bias=True)
        self.resid_dropout = nn.Dropout(dropout)
        self.n_heads = n_heads
        self.dropout = dropout
        self.causal = causal # True for LLMs, False for VIT/MAEs

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None):
        """x :: (B, t, E), k :: (B, t2, E) | None"""
        kv = x if kv is None else kv # self attention vs cross attention is decided here.
        assert len(x.shape) == 3 and len(kv.shape) == 3, (x, kv)
        B, t, t2, E = x.shape[0], x.shape[1], kv.shape[1], x.shape[2]
        q, k, v = self.q(x), self.k(kv), self.v(kv) # (B, t, E), (B, t2, E), (B, t2, E)
        # Split them in equal parts for multi head attention
        q = q.view(B, t, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t, hs)
        k = k.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)
        v = v.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)

        dropout_p = self.dropout if self.training else 0
        y = F.scaled_dot_product_attention(q, k, v, attn_mask=None, dropout_p=dropout_p, is_causal=self.causal)
        y = y.transpose(1, 2).contiguous().view(B, t, E) # (B, t, E)

        # output projection
        y = self.resid_dropout(self.c_proj(y)) # (B, t, E)
        return y

class Block(nn.Module):
    """One block of MultiHeaded attention (communication) and MLP (processing)"""
    def __init__(self, emb_size: int, n_heads: int, ff_multiplier: float, dropout: int, causal: bool, mha_type: type):
        super().__init__()
        self.mha = mha_type(emb_size, n_heads, dropout, causal)
        self.ff = nn.Sequential(nn.Linear(emb_size, round(emb_size * ff_multiplier)), nn.GELU(),
                                nn.Linear(round(emb_size * ff_multiplier), emb_size), nn.Dropout(dropout))
        self.ln1 = nn.LayerNorm(emb_size)
        self.ln2 = nn.LayerNorm(emb_size)

    def forward(self, x: tr.Tensor, *args, **kwargs) -> tr.Tensor:
        y1 = x + self.mha(self.ln1(x), *args, **kwargs)
        y2 = y1 + self.ff(self.ln2(y1))
        return y2

class Transformer(nn.Module):
    """
    Transformer class. Stacks N blocks on top of each other. Also sets up parameters and cross attention if needed
    Parameters:
    - emb_size: The shape of the embedding, maintained in all the blocks (width of the network).
    - n_blocks: The number of MultiHeadAttention-FeedForward (MHA-FF) blocks (depth of the network).
    - n_heads: The number of heads in each MHA layer.
    - dropout: Dropout of each MHA-FF layer. Applied once in MHA, once after MHA and then again in FF.
    - causal: Whether the Attention tokens attend to previous tokens or not (LM vs VIT/BERT).
    - return_all_states: Whether the forward method returns all the MHA-FF outputs or just the last one (used in DPT).
    - mha_type: The type of the MHA layer, defaults to MultiHeadAttention (using torch's FlashAttention).
    """
    def __init__(self, emb_size: int, n_blocks: int, n_heads: int, ff_multiplier: float, dropout: int,
                 causal: bool, return_all_states: bool, mha_type: list[type] | list[str] | None = None):
        super().__init__()
        self.emb_size = emb_size
        mha_type = [MultiHeadAttention] * n_blocks if mha_type is None else mha_type # default to n identical blocks.
        mha_type = [globals()[x] if isinstance(x, str) else x for x in mha_type] # convert strings to types if needed.
        assert len(mha_type) == n_blocks, (len(mha_type), n_blocks)
        self.blocks = nn.ModuleList(Block(emb_size, n_heads, ff_multiplier, dropout, causal, mha_type[i])
                                    for i in range(n_blocks))
        self.causal = causal
        self.return_all_states = return_all_states
        self.reset_parameters()

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None):
        res = [x]
        for i, block in enumerate(self.blocks):
            res.append(block(res[-1], kv if i == 0 else None)) # by default only 1st layer gets kv.
        return res[1:] if self.return_all_states else res[-1]

    def _init_weights(self, module: nn.Module):
        if isinstance(module, nn.Linear):
            nn.init.normal_(module.weight, mean=0.0, std=0.02)
            if module.bias is not None:
                nn.init.zeros_(module.bias)
        elif isinstance(module, nn.LayerNorm):
            module.weight.data.fill_(1.0)
            module.bias.data.zero_()
        elif isinstance(module, nn.Embedding):
            nn.init.trunc_normal_(module.weight.data, mean=0, std=0.02)

    def reset_parameters(self):
        self.apply(self._init_weights)

def test_transformer():
    B, t, E = 5, 16, 100

    # testing the model itself w/ regular projected inputs
    nn1 = Transformer(emb_size=E, n_blocks=1, n_heads=2, ff_multiplier=1, dropout=0.1,
                      causal=False, return_all_states=False)
    x = tr.randn(B, t, E)
    assert (y := nn1(x)).shape == (B, t, E), y

    nn2 = Transformer(emb_size=E, n_blocks=2, n_heads=E // 2, ff_multiplier=1, dropout=0.1,
                      causal=False, return_all_states=False)
    assert (y2 := nn2(x)).shape == (B, t, E), y2

    try:
        _ = Transformer(emb_size=E, n_blocks=2, n_heads=2 * E, ff_multiplier=1, dropout=0.1,
                        causal=False, return_all_states=False)
    except AssertionError as e:
        assert f"({E} % {2 * E}) == 0" in f"{e}"

    nn3 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=2, dropout=0.1,
                      causal=False, return_all_states=False)
    assert (y3 := nn3(x)).shape == (B, t, E), y3

    try:
        _ = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=-1,
                        causal=False, return_all_states=False)
    except ValueError as e:
        assert "dropout probability has to be between 0 and 1, but got -1" in f"{e}"

    nn4 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=False, return_all_states=False)
    assert (y4_1 := nn4(x, kv=x)).shape == (B, t, E), y4_1
    assert (y4_2 := nn4(x, kv=tr.randn(B, t * 2, E))).shape == (B, t, E), y4_2 # xatt allows kv of different t shape !
    assert (y4_2 := nn4(x, kv=y3)).shape == (B, t, E), y4_2 # xatt allows other transformer outputs as well in kv

    nn5 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=True, return_all_states=False) # causal (iGPT or LMs)
    assert (y5 := nn5(x)).shape == (B, t, E), y5

    nn6 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=False, return_all_states=True)
    assert len(y6 := nn6(x)) == 2 and all(_y.shape == (B, t, E) for _y in y6), y6

    # MHA Vanilla tests
    nnv_1 = Transformer(emb_size=E, n_blocks=1, n_heads=2, ff_multiplier=1, dropout=0.1,
                        causal=False, return_all_states=False,
                        mha_type=[MultiHeadAttentionVanilla])
    assert (yv_1 := nnv_1(x)).shape == (B, t, E), yv_1

    nnv_2 = Transformer(emb_size=E, n_blocks=1, n_heads=2, ff_multiplier=1, dropout=0.1,
                        causal=True, return_all_states=False,
                        mha_type=[MultiHeadAttentionVanilla])
    assert (yv_2 := nnv_2(x)).shape == (B, t, E), yv_2

    nnv_3 = Transformer(emb_size=E, n_blocks=1, n_heads=2, ff_multiplier=1, dropout=0.1,
                        causal=False, return_all_states=False,
                        mha_type=[MultiHeadAttentionVanilla])
    assert (yv_3 := nnv_3(x, kv=tr.randn(B, t * 2, E))).shape == (B, t, E), yv_3
