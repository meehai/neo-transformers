"""Extends MHA to MultiTask. Has one q, k, v, c_proj layers for each task"""
from torch import nn
from torch.nn import functional as F
import torch as tr

from .transformer import MultiHeadAttention

class MultiTaskMHA(MultiHeadAttention):
    """Extends MHA to MultiTask. Has one q, k, v, c_proj layers for each task. Does x-attention: t_i x all(t_j)"""
    def __init__(self, *args, n_tasks: int, **kwargs):
        self.n_tasks = n_tasks
        super().__init__(*args, **kwargs)
        self.emb_size = self.q.in_features
        del self.q, self.k, self.v, self.c_proj
        self.q = nn.ModuleList(nn.Linear(self.emb_size, self.emb_size, bias=False) for _ in range(self.n_tasks))
        self.k = nn.ModuleList(nn.Linear(self.emb_size, self.emb_size, bias=False) for _ in range(self.n_tasks))
        self.v = nn.ModuleList(nn.Linear(self.emb_size, self.emb_size, bias=False) for _ in range(self.n_tasks))
        self.c_proj = nn.ModuleList(nn.Linear(self.emb_size, self.emb_size, bias=True) for _ in range(self.n_tasks))

    def fwd_attn(self, x: tr.Tensor, kv: tr.Tensor, task_ix: int) -> tr.Tensor:
        """This can be overriden with other q, k, v projection layers"""
        assert len(x.shape) == 3 and (kv is None or len(kv.shape) == 3), (x, kv)
        B, t, t2, E = x.shape[0], x.shape[1], kv.shape[1] if kv is not None else x.shape[1], x.shape[2]
        q, k, v = self.q[task_ix](x), self.k[task_ix](kv), self.v[task_ix](kv) # (B, t, E), (B, t2, E), (B, t2, E)
        # Split them in equal parts for multi head attention
        q = q.view(B, t, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t, hs)
        k = k.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)
        v = v.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)

        dropout_p = self.dropout if self.training else 0
        y = F.scaled_dot_product_attention(q, k, v, attn_mask=None, dropout_p=dropout_p, is_causal=self.causal)
        y = y.transpose(1, 2).contiguous().view(B, t, E) # (B, t, E)
        y = self.resid_dropout(self.c_proj[task_ix](y)) # (B, t, E). Output projection + final dropout.
        return y

    def forward(self, x: tr.Tensor, mask_task: tr.Tensor):
        assert len(x.shape) == 3, f"Expected (B, t, E={self.emb_size}), got {x.shape}"
        B, t, E = x.shape
        assert len(mask_task.shape) == 1 and len(mask_task) == t, f"must be eq across batch for eff: {mask_task}, {t}"

        y = tr.zeros_like(x)
        for task_ix in range(self.n_tasks):
            x_task = x[:, mask_task == task_ix].view(B, -1, E) # (B, t_tk[i], E)
            x_others = x[:, mask_task != task_ix].view(B, -1, E) # (B, t_tk[j!=i], E]
            y[:, mask_task == task_ix] = self.fwd_attn(x_task, x_others, task_ix) # (B, t_tk[i], E)
        return y

def test_MultiTaskMHA():
    B, t, E, n_tasks = 5, 10, 20, 3
    mha = MultiTaskMHA(n_tasks=5, emb_size=E, n_heads=2, causal=False, dropout=0)

    x = tr.randn(B, t, E)
    mask_task = tr.randint(0, n_tasks, size=(t, ))
    y = mha(x, mask_task=mask_task)
    assert y.shape == (B, t, E)
