import math
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1] / "utils"))
from torch import nn
import torch as tr

from utils import split_pred_tensor, positionalencoding2d, patch_tensor, unpatch_with_size, mask_tensor
from .layers.transformer import Transformer

class ViTMMAE0(nn.Module):
    def __init__(self, patch_size: tuple[int, int], tasks_ch: dict[str, int], data_size: tuple[int, int],
                 emb_size: int, dropout: int,
                 enc_n_blocks: int, enc_n_heads: int, enc_ff_multiplier: float,
                 dec_n_blocks: int, dec_n_heads: int, dec_ff_multiplier: float,):
        super().__init__()
        self.emb_size = emb_size
        self.patch_size = patch_size
        self.data_size = data_size
        self.tasks_ch = tasks_ch
        self.max_sequence_size = int(self.data_size[0] * self.data_size[1] / self.patch_size[0] / self.patch_size[1])
        self.proj_in = nn.ModuleDict({k: nn.Linear(v * math.prod(patch_size), emb_size)
                                      for k, v in tasks_ch.items()})
        self.encoder = Transformer(emb_size=emb_size, n_blocks=enc_n_blocks, n_heads=enc_n_heads,
                                   ff_multiplier=enc_ff_multiplier, dropout=dropout, causal=False,
                                   return_all_states=False)

        self.decoders = nn.ModuleDict({k: Transformer(emb_size=emb_size, n_blocks=dec_n_blocks, n_heads=dec_n_heads,
                                                      ff_multiplier=dec_ff_multiplier, dropout=dropout, causal=False,
                                                      return_all_states=False) for k in tasks_ch.keys()})
        self.proj_out = nn.ModuleDict({k: nn.Linear(emb_size, v * math.prod(patch_size))
                                       for k, v in tasks_ch.items()})

        self.mask_tokens = nn.ParameterDict()
        for k in tasks_ch.keys():
            self.mask_tokens[k] = nn.Parameter(tr.randn(1, 1, emb_size))
            nn.init.normal_(self.mask_tokens[k], std=0.02)
        self.pe_2d = nn.Parameter(positionalencoding2d(self.emb_size, data_size[0] // patch_size[0],
                                                       data_size[1] // patch_size[1]), requires_grad=False)

    def encoder_fwd(self, x_masked: dict[str, tr.Tensor], masks: dict[str, tr.Tensor]) -> dict[str, tr.Tensor]:
        B = len(next(iter(x_masked.values())))
        t_tk = {k: tr.where(~masks[k])[0] for k in masks}
        t_tk_2d = {k: (t_tk[k] // self.pe_2d.shape[1], t_tk[k] % self.pe_2d.shape[1]) for k in t_tk}
        x_pe_2d_enc = {k: self.pe_2d[t_tk_2d[k][0], t_tk_2d[k][1]] for k in t_tk.keys()}

        x_flat = {k: v.reshape(B, self.proj_in[k].in_features, -1).permute(0, 2, 1)
                  for k, v in x_masked.items()} # {tk: (B, t_task, (C*H*W))}
        x_proj = {k: self.proj_in[k](x_flat[k]) for k in x_flat.keys()} # {tk: (B, t_tk, E)}
        x_proj_pos = {k: x_proj[k] + x_pe_2d_enc[k] for k in x_flat.keys()} # {tk: (B, t_tk, E)}
        x_cat = tr.cat(list(x_proj_pos.values()), dim=1) # (B, t, E)
        y_encoder_cat = self.encoder(x_cat) # (B, t, E)
        y_split = split_pred_tensor(y_encoder_cat, {k: v.shape[1] for k, v in x_proj.items()}) # {tk: (B, t_tk, E)}
        return y_split

    def patch_and_mask(self, x: dict[str, tr.Tensor], drop_percent: dict[str, float]) \
            -> tuple[dict[str, tr.Tensor], dict[str, tr.Tensor]]:
        x_patched = {k: patch_tensor(x[k], self.patch_size) for k in x.keys()}
        x_masked_mask = {k: mask_tensor(v, drop_percent[k]) for k, v in x_patched.items()}
        x_masked = {task: x_masked_mask[task][0] for task in x_masked_mask}
        masks = {task: x_masked_mask[task][1] for task in x_masked_mask} # store original masks for return/metrics
        return x_masked, masks

    def forward(self, x: dict[str, tr.Tensor], drop_percent: dict[str, float]) -> dict[str, tr.Tensor]:
        """x::{task: (B, C, H, W, t)}"""
        B = len(next(iter(x.values())))

        # encoder
        x_masked, masks = self.patch_and_mask(x, drop_percent)
        y_encoder = self.encoder_fwd(x_masked, masks)

        # prepare for decoder (re-assemble masked/unmasked patches) and pass through it
        y_full = {}
        t_tk = {k: tr.where(~masks[k])[0] for k in masks}
        for k in self.mask_tokens.keys():
            y_full[k] = self.mask_tokens[k].repeat(B, self.max_sequence_size, 1) # {tk: (B, T, E)}
            y_full[k][:, t_tk[k]] = y_encoder[k]
            y_full[k] += self.pe_2d.reshape(1, -1, self.emb_size)
        y_decoded = {k: self.decoders[k](y_full[k]) for k in y_full.keys()} # {tk: (B, T, E)}
        y_proj = {k: self.proj_out[k](y_decoded[k]) for k in y_decoded.keys()} # {tk: (B, (C*H*W), T)}

        # final permutes
        y_patched = {k: v.permute(0, 2, 1).reshape(B, self.tasks_ch[k], *self.patch_size, self.max_sequence_size)
                     for k, v in y_proj.items()} # {k: (B, C, H, W, T)
        y = {k: unpatch_with_size(y_patched[k], x[k].shape) for k in y_patched} # {k: (B, C, H, W)}
        return y, masks # {k: (B, C, H, W)}, {k: t_tk}
