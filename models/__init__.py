"""init file"""
import math
from omegaconf import DictConfig
from torch import nn

from .safeuav import SafeUAV
from .vit_mmae0 import ViTMMAE0
from .convit_mae0 import ConViTMAE0

def build_model(cfg: DictConfig) -> nn.Module:
    if cfg.model.type == "safeuav":
        cfg.model.parameters.in_channels = sum(x[0] for x in cfg.model.hparams.data_shape.values())
        cfg.model.parameters.out_channels = sum(x[0] for x in cfg.model.hparams.data_shape.values())
        return SafeUAV(**cfg.model.parameters)
    if cfg.model.type in ("vit_mmae0", "convit_mae0"):
        cfg.model.parameters.tasks_ch = {k: v[0] for k, v in cfg.model.hparams.data_shape.items()}
        cfg.model.parameters.data_size = next(iter(cfg.model.hparams.data_shape.values()))[1:3]
        ps, ds = cfg.model.parameters.patch_size, cfg.model.parameters.data_size
        assert ds[0] % ps[0] == 0 and ds[1] % ps[1] == 0, (ds, ps)
        if cfg.model.type == "vit_mmae0":
            return ViTMMAE0(**cfg.model.parameters)
        if cfg.model.type == "convit_mae0":
            return ConViTMAE0(**cfg.model.parameters)
    raise ValueError(f"Unknown model: '{cfg.model.type}'")
