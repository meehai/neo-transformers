import math
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parents[1] / "utils"))
from torch import nn
import torch as tr

from utils import split_pred_tensor, positionalencoding2d, patch_tensor, mask_tensor, unpatch_with_size
from .layers.transformer import Transformer
from .vit_mmae0 import ViTMMAE0

class ConViTMAE0(ViTMMAE0):
    def __init__(self, patch_size: tuple[int, int], tasks_ch: dict[str, int], data_size: tuple[int, int],
                 emb_size: int, dropout: int, conv_layers: int, conv_n_filters: int,
                 enc_n_blocks: int, enc_n_heads: int, enc_ff_multiplier: float,
                 dec_n_blocks: int, dec_n_heads: int, dec_ff_multiplier: float):
        nn.Module.__init__(self)
        self.emb_size = emb_size
        self.data_size = data_size
        self.patch_size = patch_size
        self.tasks_ch = tasks_ch
        self.max_sequence_size = int(self.data_size[0] * self.data_size[1] / self.patch_size[0]
                                     / self.patch_size[1] * len(self.tasks_ch))

        filters = [sum(tasks_ch.values()), *([conv_layers] * (conv_n_filters - 1)), sum(tasks_ch.values())]
        self.conv_in = nn.ModuleList([nn.Sequential(nn.Conv2d(i, o, kernel_size=3, padding=1), nn.ReLU())
                                      for i, o in zip(filters[0:-1], filters[1:])])

        self.proj_in = nn.ModuleDict({k: nn.Linear(v * math.prod(patch_size), emb_size) for k, v in tasks_ch.items()})
        self.encoder = Transformer(emb_size=emb_size, n_blocks=enc_n_blocks, n_heads=enc_n_heads, causal=False,
                                   ff_multiplier=enc_ff_multiplier, dropout=dropout, return_all_states=False)

        self.decoder = Transformer(emb_size=emb_size, n_blocks=dec_n_blocks, n_heads=dec_n_heads, causal=False,
                                   ff_multiplier=dec_ff_multiplier, dropout=dropout, return_all_states=False)
        self.proj_out = nn.ModuleDict({k: nn.Linear(emb_size, v * math.prod(patch_size))
                                       for k, v in tasks_ch.items()})
        self.mask_token = nn.Parameter(tr.randn(1, 1, self.emb_size))
        nn.init.normal_(self.mask_token, std=0.02)
        self.pe_2d = nn.Parameter(positionalencoding2d(self.emb_size, data_size[0] // patch_size[0],
                                                       data_size[1] // patch_size[1]), requires_grad=False)

    def forward(self, x: dict[str, tr.Tensor], drop_percent: dict[str, float]) -> dict[str, tr.Tensor]:
        """x::{task: (B, C, H, W, t)}"""
        B = len(next(iter(x.values())))
        # The initial convolutional layers
        y_conv = tr.cat(list(x.values()), dim=1)
        for conv in self.conv_in:
            y_conv = conv(y_conv)
        y_conv_split = split_pred_tensor(y_conv,  self.tasks_ch)

        # encoder
        y_conv_masked, masks = self.patch_and_mask(y_conv_split, drop_percent)
        y_encoder = self.encoder_fwd(y_conv_masked, masks)

        # prepare for decoder (re-assemble masked/unmasked patches) and pass through it
        y_full = {}
        t_tk = {k: tr.where(~masks[k])[0] for k in masks}
        T_max_tk = self.max_sequence_size // len(self.tasks_ch)
        for k in y_encoder.keys():
            y_full[k] = self.mask_token.repeat(B, T_max_tk, 1) # {tk: (B, T/#tk, E)}
            y_full[k][:, t_tk[k]] = y_encoder[k]
            y_full[k] += self.pe_2d.reshape(1, -1, self.emb_size)
        y_full_stacked = tr.cat(list(y_full.values()), dim=1)
        y_dec = self.decoder(y_full_stacked)

        # final permutes
        y_dec_split = split_pred_tensor(y_dec, {k: T_max_tk for k in self.tasks_ch})
        y_proj = {k: self.proj_out[k](v) for k, v in y_dec_split.items()}
        y_patched = {k: v.permute(0, 2, 1).reshape(B, -1, *self.patch_size, T_max_tk) for k, v in y_proj.items()}
        y = {k: unpatch_with_size(v, (B, self.tasks_ch[k], *self.data_size)) for k, v in y_patched.items()}
        return y, masks # {k: (B, C, H, W)}, {k: t_tk}
