"""Plot functions for various models in this repo"""
from __future__ import annotations
from pathlib import Path
from typing import Callable
from lightning_module_enhanced import LME
from vre.utils import collage_fn, image_resize, image_write, image_add_border, MemoryData
from vre.representations import Representation, ReprOut
from vre.readers.multitask_dataset import MultiTaskItem
import torch as tr
import numpy as np

from algorithms.mae_algorithms import MAEAlgorithmOutput

def vre_plot_fn(x: tr.Tensor, node: Representation) -> np.ndarray:
    assert len(x.shape) == 3, (node, x.shape)
    node.data = ReprOut(None, MemoryData(x.cpu().numpy()[None]), [0])
    res = node.make_images(node.data)[0]
    return res

def _vit_plot_one_item(x: dict[str, tr.Tensor], pred: dict[str, tr.Tensor], unkept_ix: dict[str, tr.Tensor],
                       plot_fns: dict[str, Callable[[tr.Tensor], np.ndarray]],
                       patch_shape: tuple[int, int]) -> np.ndarray:
    all_imgs = []
    assert all(x[k].shape == pred[k].shape for k in x.keys()), f"{x} \n----\n {pred}"
    tasks = list(x.keys())
    ph, pw = patch_shape
    h, w = x[tasks[0]].shape[1:3]
    nh, nw = h // ph, w // pw
    assert nh > 0 and nw > 0, (nh, nw, ph, pw, h, w)

    for task in tasks:
        x_t, y_t, plot_fn, unkept_t = x[task], pred[task].detach(), plot_fns[task], unkept_ix[task]
        x_img, y_img = plot_fn(x_t.permute(1, 2, 0)), plot_fn(y_t.permute(1, 2, 0)) # (h, w, 3)
        assert (a := x_img.shape) == (h, w, 3) and y_img.shape == (h, w, 3), (task, a, y_img.shape, (h, w, 3))

        for kept in unkept_t: # loop only through the masked (unkept) patches and cross them out
            i, j = (kept // nw).item(), (kept % nw).item()
            li, ri, lj, rj = i * ph, (i + 1) * ph, j * pw, (j + 1) * pw
            # out_img[li: ri, lj: rj] = y_img[li: ri, lj: rj]
            x_img_patch = x_img[li: ri, lj: rj]
            border_color = [255, 0, 0] if x_img_patch.sum() else [0, 0, 255]
            border_thicc = max(2, min(7, max(x_img_patch.shape[0:2]) // 33))
            x_img[li: ri, lj: rj] = image_add_border(x_img_patch, color=border_color, thicc=border_thicc, add_x=True)
        all_imgs.extend([x_img, y_img])
    collage = collage_fn(all_imgs)
    return image_resize(collage, height=min(max(collage.shape[0], 1024), 3078), width=None)

# pylint: disable=unused-argument
def mae_plot_fn(model: LME, batch: MultiTaskItem, y: MAEAlgorithmOutput, out_dir: Path | None,
                plot_fns: dict[str, Callable]) -> np.ndarray:
    """
    Make plots at the end of the epoch for VIT Model predictions
    Parameters:
    - model: The model used to generate the predictions
    - batch: A dict with the following keys: 'data', 'patched_data', 'tasks' and 'image_shape'
      - 'data': {task: [(ch[task], h, w)]} a dict with lists of size B
      - 'tasks': A list of all the tasks
      - 'image_shape' ({task: ch[task]}, h, w)
    - y: A dictionary of {task: (masked_tensor, mask)} for each task.
    - out_dir: Where to store the collage of each image
    - plot_fns: {task: fn} that receives a prediction (ch[task], h, w) and outputs a (h, w, 3) image
    """
    mb, tasks = len(batch["data"][list(batch["data"].keys())[0]]), batch["data"].keys()
    if out_dir is not None: out_dir.parent.mkdir(exist_ok=True, parents=True)
    ph, pw = (next(iter(y.gt.values())))[0].shape[-3:-1]
    unkept_ix = {task: tr.where(y.y.mask[task] == True)[0] for task in y.y.mask.keys()}
    for i in range(n_imgs := min(mb, 1)):
        unpatched_x_b = {k: batch["data"][k][i] for k in tasks}
        unpatched_pred_b = {k: v[i] for k, v in y.y.pred.items()}
        collage = _vit_plot_one_item(unpatched_x_b, unpatched_pred_b, unkept_ix, plot_fns, (ph, pw))
        if out_dir is not None:
            image_write(collage, f"{out_dir}{'' if n_imgs == 1 else f'_{i}'}.png") # epoch_i.png or just epoch.png
    return collage

def ff_plot_fn(model: LME, batch: MultiTaskItem, y: tr.Tensor, out_dir: Path, plot_fns: dict[str, Callable]):
    raise NotImplementedError

def test_vit_mae_algo_plot_fn():
    from utils import patch_tensor, mask_tensor
    from types import SimpleNamespace
    from algorithms.mae_algorithms import MAEAlgorithmOutput, YPredMask
    def _to_image(x: tr.Tensor) -> np.ndarray:
        assert len(x.shape) == 3, x.shape
        y = ((x - (_min := x.min())) / (x.max() - _min)).nan_to_num(0)
        y = y[..., 0:1] if y.shape[-1] != 3 else y
        y = y.repeat((1, 1, 3)) if y.shape[-1] == 1 else y
        return (y * 255).byte().cpu().numpy()

    bs, (ph, pw) = 2, (30, 30)
    ch, h, w = {"A": 1, "B": 2}, 990, 420
    tasks = ["A", "B"]
    x_stack = {task: tr.randn(bs, ch[task], h, w) for task in tasks}
    patched_data = {task: patch_tensor(x_stack[task], ((ph, pw))) for task in tasks}
    masked_and_kept_ix_data = {task: mask_tensor(patched_data[task], drop_percent=50) for task in tasks}
    masks = {task: masked_and_kept_ix_data[task][1] for task in tasks}

    model = SimpleNamespace(hparams=SimpleNamespace(tasks=["A", "B"], patch_size=(30, 30)))
    batch = {"data": x_stack, "image_shape": (ch, h, w)}
    y_algo = MAEAlgorithmOutput(y=YPredMask(x_stack, masks), metrics=None, x=x_stack, gt=patched_data)
    collage = mae_plot_fn(model, batch, y_algo, plot_fns={"A": _to_image, "B": _to_image}, out_dir=None)
    assert collage.shape == (1980, 840, 3)
